﻿#include "jptwatercut.h"
#include "ui_jptwatercut.h"
#include <QFileDialog>
#include <QDebug>
#include <algorithm>
#include <QVector>
#include <QFile>
#include <math.h>

//Esta variable es importante para el tema de que OS se esta usando
//True para windows, false para Linux
static bool windows = true;

static bool laminar=false, lei=false, turbulent=false, turb_lam=false;
static int vector_values_calibration[8];
//Average--res_oil[0],res_air[1],res_water[2],res_f_water[3]
//Average--cap_oil[4],cap_air[5],cap_water[6],cap_f_water[7]
static int vector_high_low_wc[12];
//Percent--hwc_up[0],Value--hwc_up[1]
//Percent--hwc_dw[2],Value--hwc_dw[3]
//Percent--lwc_up[4],Value--lwc_up[5]
//Percent--lwc_dw[6],Value--lwc_dw[7]
//Percent--hwc_up2[8],Value--hwc_up2[9]
//Percent--hwc_dw2[10],Value--hwc_dw2[11]
static QString vector_delta_normalization[12];

static int on_off_inverse, on_off_sali, on_off_temp, on_off_tempval;
static float value_tempval;
//On->1, Off->0
static int vector_inverse_values[12];
static QVector<jptabssensor *> vector_abs_sensors;

//static QVector<QList<int>>vector_res_norma;
//static QVector<QList<int>>vector_capa_norma;

//Listas para obtener valores necesarios
static QList<QList<float>>vector_watercut_resistivity;
static QList<QList<float>>vector_watercut_resistivity_ei;
static QList<QList<float>>vector_watercut_capacitance;
static QList<QList<float>>vector_watercut_resistivity_turb;
static QList<QList<float>>vector_watercut_capacitance_turb;
static QList<QList<float>>vector_watercut_resistivity_lam_turb;
static QList<QList<float>>vector_watercut_capacitance_lam_turb;
static QList<QList<int>>vector_res_salinity;
static QList<QList<int>>vector_capa_salinity;
static QList<int>vector_real_salinity;
static QList<QList<int>>vector_res_temp;
static QList<QList<int>>vector_capa_temp;
static QList<QList<float>>vector_res_inverse_emulsion;
static QList<QList<float>>vector_cap_inverse_emulsion;
static QList<QList<float>>vector_cap_inverse_emulsion_final;

static QVector<QLineEdit *> vector_line_edit_norm;

//area de los cortes
const double area_sen[6] = {0.17,0.15,0.15,0.17,0.15,0.21};

//Watercuts
static float res_laminar_EI =0;
static float cap_laminar_EI =0;
static float res_laminar=0;
static float cap_laminar=0;
static float res_turbulent=0;
static float cap_turbulent=0;
static float res_lam_turbulent=0;
static float cap_lam_turbulent=0;

static QVector<QList<node_norm>> _node_norm;

static float caudal_min =0;

jptwatercut::jptwatercut(QWidget *parent) : QMainWindow(parent) , ui(new Ui::jptwatercut){
    ui->setupUi(this);

    //Tamaño de la ventana
    setFixedSize(800,600);
    setWindowTitle("Analyzer SWC");

    ui->line_path->setText("/");

    connect(ui->btn_select_db, SIGNAL(clicked()),this, SLOT(select_db()));

    connect(ui->date_init, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(get_datetime_init(QDateTime)));
    connect(ui->date_end, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(get_datetime_end(QDateTime)));

    connect(ui->btn_test, SIGNAL(clicked()), this, SLOT(test_db()));

    //New models, modelos que nos permiten el acceso a las diferentes clases
    database = new jptdatabase();
    model = new jptmodel();
    sensors = new jptsensors();
    abs_sensors = new jptabssensor();
    cap_sensors = new jptcapsensor();
    res_sensors = new jptressensor();

    //Carga los modelos de temperatura
    charge_models_tempe_pres();

    //Fechas iniciales en el sistema
    QDateTime i(QDate(2019,11,14),QTime(7,55,00));
    QDateTime e(QDate(2019,11,14),QTime(9,55,00));

    //Enrutamiendo inicial de la base de datos
    if(windows){
        ui->line_path->setText("C:/JPT/jptdatabase.db");
    }else{
        ui->line_path->setText("/home/miguel-jpt/Dev/Projects_Qt/jptwatercut_v4/jptdatabase.db");
    }

    //Seteo de las fechas en los labels
    ui->date_init->setDateTime(i);
    ui->date_end->setDateTime(e);

    //Carga un vector de los edits de normalizacion
    charge_line_norm();
    //Validadores de los edits
    //validator_line_norm();

    //Read init calibration values
    read_txt_values_calibration();
    //Read init hlwc values
    read_txt_values_high_low_wc();
    //Read init delta normalization values
    read_txt_delta_normalization();
    //Read init charge inverse on, off
    read_txt_inverse();
    //Read init inverse values
    read_txt_inverse_values();
    //Read init charge sali on, off
    read_txt_correct_sali();
    //Read init charge temp on, off
    read_txt_correct_temp();
    //Read init charge temp_val on, off and value
    read_txt_correct_tempval();

    fill_combo_salinity();

    //hacemos que los line no sean editables
    ui->line_result_sali->setReadOnly(true);
    ui->line_result_wclc->setReadOnly(true);
    ui->line_result_wclr->setReadOnly(true);
    ui->line_result_wclt->setReadOnly(true);
    ui->line_result_wclei->setReadOnly(true);
    ui->line_result_temp->setReadOnly(true);
    ui->line_result_wctr->setReadOnly(true);
    ui->line_result_wctc->setReadOnly(true);
    ui->line_result_wctrt->setReadOnly(true);
    ui->line_result_wcltr->setReadOnly(true);

    connect(ui->btn_exe, SIGNAL(clicked()), this, SLOT(execute()));
    connect(ui->btn_laminar_EI, SIGNAL(clicked()), this, SLOT(execute_laminar_EI()));
    connect(ui->btn_turbulent, SIGNAL(clicked()), this, SLOT(regimen_turbulent()));
    connect(ui->btn_turbulent_lam, SIGNAL(clicked()), this, SLOT(regimen_turbulent_laminar()));
    connect(ui->btn_create_excel, SIGNAL(clicked()), this, SLOT(create_excel()));
    //Por ahora se deja solamente en off
    on_off_inverse=0;

    connect(ui->btn_save_norm, SIGNAL(clicked()), this, SLOT(save_txt_delta_normalization()));

    connect(database, SIGNAL(kill_thread()), this, SLOT(kill_thread()));

    normalization = new jptnormalization();

    //qRegisterMetaType<QVector<QList<node_norm>>>();

    connect(normalization, SIGNAL(send_data_norm()), this, SLOT(get_norm()));

    connect(ui->btn_calculate_special, SIGNAL(clicked()), this, SLOT(calculate_caudal()));
    connect(ui->check_th_gas, SIGNAL(clicked()), this, SLOT(active_less_gas()));


}

//Cierra la interfaz
jptwatercut::~jptwatercut(){
    delete ui;
}

//Le agrega un vector de tipo edit, para mejor manipulacion
void jptwatercut::charge_line_norm(){
    vector_line_edit_norm.append(ui->line_norm_res1);
    vector_line_edit_norm.append(ui->line_norm_res2);
    vector_line_edit_norm.append(ui->line_norm_res3);
    vector_line_edit_norm.append(ui->line_norm_res4);
    vector_line_edit_norm.append(ui->line_norm_res5);
    vector_line_edit_norm.append(ui->line_norm_res6);
    vector_line_edit_norm.append(ui->line_norm_cap1);
    vector_line_edit_norm.append(ui->line_norm_cap2);
    vector_line_edit_norm.append(ui->line_norm_cap3);
    vector_line_edit_norm.append(ui->line_norm_cap4);
    vector_line_edit_norm.append(ui->line_norm_cap5);
    vector_line_edit_norm.append(ui->line_norm_cap6);
}

//Al vector de tipo edit, para agregar el validador a los edits
void jptwatercut::validator_line_norm(){
    for (int i=0;i<vector_line_edit_norm.size();i++) {
        vector_line_edit_norm[i]->setValidator(new QDoubleValidator(this));
    }
}

//Limpia todas las variables del sistema
void jptwatercut::clean_all(){

    //vector_res_norma.clear();
    //vector_capa_norma.clear();
    vector_real_salinity.clear();
    vector_res_salinity.clear();
    vector_capa_salinity.clear();
    vector_watercut_resistivity.clear();
    vector_watercut_resistivity_ei.clear();
    vector_watercut_capacitance.clear();
    vector_watercut_resistivity_turb.clear();
    vector_watercut_capacitance_turb.clear();
    vector_watercut_resistivity_lam_turb.clear();
    vector_watercut_capacitance_lam_turb.clear();
    vector_res_temp.clear();
    vector_capa_temp.clear();
    vector_res_inverse_emulsion.clear();
    vector_cap_inverse_emulsion.clear();
    vector_cap_inverse_emulsion_final.clear();
    on_off_inverse=0;

    laminar=false;
    lei=false;
    turbulent=false;
    turb_lam=false;

    res_laminar_EI =0;
    cap_laminar_EI =0;
    res_laminar=0;
    cap_laminar=0;
    res_turbulent=0;
    cap_turbulent=0;
    res_lam_turbulent=0;
    cap_lam_turbulent=0;

    caudal_min = 0;


    ui->line_result_sali->setText("");
    ui->line_result_wclc->setText("");
    ui->line_result_wclr->setText("");
    ui->line_result_wclt->setText("");
    ui->line_result_wclei->setText("");
    ui->line_result_temp->setText("");
    ui->line_result_wctr->setText("");
    ui->line_result_wctc->setText("");
    ui->line_result_wctrt->setText("");
    ui->line_result_wcltr->setText("");
    ui->line_percent_lam->setText("");
    ui->line_percent_lei->setText("");
    ui->line_percent_turb->setText("");
    ui->line_percent_turb_lam->setText("");
}

//llena la lista deplegable para las curvas de salinidad
void jptwatercut::fill_combo_salinity(){
    QStringList fill_combo;
    fill_combo << "-------" <<"Curve 1 100 a 7000 PPM" <<"Curve 2 600 a 7500 PPM" <<"Curve 3 1100 a 8000 PPM"
               <<"Curve 4 1600 a 8500 PPM" <<"Curve 5 2100 a 9000 PPM" <<"Curve 6 2600 a 9500 PPM"
              <<"Curve 7 3100 a 10000 PPM" <<"Curve 8 3600 a 10500 PPM" <<"Curve 9 4100 a 11000 PPM"
             <<"Curve 10 4600 a 11500 PPM" <<"Curve 11 5100 a 12000 PPM";
    ui->combo_models_salinity->addItems(fill_combo);

    connect(ui->combo_models_salinity,SIGNAL(currentTextChanged(QString)), this, SLOT(select_combo_salinity(QString)));

    //aqui voy a definir la lista de la lista desplegable de diametro de tuberia
    QStringList fill_combo_tuberia;

    fill_combo_tuberia << "3''" << "2''";

    ui->combo_id_pipe->addItems(fill_combo_tuberia);

    connect(ui->combo_id_pipe,SIGNAL(currentTextChanged(QString)), this, SLOT(select_combo_pipe(QString)));

    //seteo el valor por default al pipe constant
    ui->line_pipe_constant->setText("25");
}

//cambio de lista deplegable de diametro de tuberia
void jptwatercut::select_combo_pipe(QString value){
    if(value.contains("3''")){
        ui->line_pipe_constant->setText("25");
    }else{
        ui->line_pipe_constant->setText("12.5");
    }
}

//cambio para elegir la curva de salinidad
void jptwatercut::select_combo_salinity(QString curve){

    if(curve.contains("Curve 1 100 a 7000 PPM")){
        ui->label_salinity->setText("5878.1 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 2 600 a 7500 PPM")){
        ui->label_salinity->setText("7814.6 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 3 1100 a 8000 PPM")){
        ui->label_salinity->setText("10389.1 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 4 1600 a 8500 PPM")){
        ui->label_salinity->setText("13812 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 5 2100 a 9000 PPM")){
        ui->label_salinity->setText("18362 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 6 2600 a 9500 PPM")){
        ui->label_salinity->setText("24411 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 7 3100 a 10000 PPM")){
        ui->label_salinity->setText("32453 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 8 3600 a 10500 PPM")){
        ui->label_salinity->setText("43144 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 9 4100 a 11000 PPM")){
        ui->label_salinity->setText("57357 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 10 4600 a 11500 PPM")){
        ui->label_salinity->setText("76253 * exp(-0.00006 * RAW)");
    }else if(curve.contains("Curve 11 5100 a 12000 PPM")){
        ui->label_salinity->setText("101374 * exp(-0.00006 * RAW)");
    }else{
        ui->label_salinity->setText("");
    }
}

//calcula el caudal y determina que modelo se puede usar
void jptwatercut::calculate_caudal(){
    caudal_min = ui->spin_bfpd->value() / ui->line_pipe_constant->text().toFloat();
    ui->line_ft_min->setText(QString::number(caudal_min, 'f', 2));

    if(ui->combo_id_pipe->itemText(0).contains("3''") && caudal_min >= 120){
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(true);
        ui->radio_lc_hf->setEnabled(true);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(false);
        ui->radio_lc_lf->setEnabled(false);
    }else if(ui->combo_id_pipe->itemText(1).contains("2''") && caudal_min >= 120){
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(true);
        ui->radio_lc_hf->setEnabled(true);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(false);
        ui->radio_lc_lf->setEnabled(false);
    }else if(ui->combo_id_pipe->itemText(1).contains("2''") && caudal_min >= 80 && caudal_min <120){
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(false);
        ui->radio_lc_hf->setEnabled(false);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(true);
        ui->radio_lc_lf->setEnabled(true);
    }else if(ui->combo_id_pipe->itemText(1).contains("2''") && caudal_min >= 40 && caudal_min <80){
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(true);
        ui->radio_lc_hf->setEnabled(true);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(true);
        ui->radio_lc_lf->setEnabled(true);
    }else if(ui->combo_id_pipe->itemText(1).contains("2''") && caudal_min < 40 ){
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(true);
        ui->radio_lc_hf->setEnabled(true);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(true);
        ui->radio_lc_lf->setEnabled(true);
    }else{
        //TURBULENTOS
        ui->radio_hc_hf->setEnabled(false);
        ui->radio_lc_hf->setEnabled(false);

        //los de laminar siguen siendo false
        ui->radio_hc_lf->setEnabled(false);
        ui->radio_lc_lf->setEnabled(false);
    }
}

//Llena unas variables iniciales en el sistema
void jptwatercut::fill_all(){
    QList<int> all;
    for (int i=0;i<=min;i++) {
        for (int j=0;j<=6;j++) {
            all.append(0);
        }
        vector_res_salinity.append(all);
        vector_capa_salinity.append(all);
        vector_res_temp.append(all);
        vector_capa_temp.append(all);
    }
}

void jptwatercut::active_less_gas(){
    if(ui->check_th_gas->isChecked()){
        ui->spin_th_gas->setEnabled(true);
    }else{
        ui->spin_th_gas->setEnabled(false);
    }
}

//Obtiene la fecha inicial
void jptwatercut::get_datetime_init(QDateTime init){
    _init = init;
}

//Obtiene la fecha final
void jptwatercut::get_datetime_end(QDateTime end){
    _end = end;
}

//Carga los modelos de temperatura para res y cap
void jptwatercut::charge_models_tempe_pres(){
    vector_abs_sensors.append(new jptabssensor);
    vector_abs_sensors.append(new jptabssensor);
}

//Carga el archivo de valores high_low_wc
void jptwatercut::read_txt_values_high_low_wc(){
    QString path("");
    if(windows){
        path = "C:/JPT/External/hlwc.txt";
    }else{
        path = "/jpt/jptwatercut/External/hlwc.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de hlwc...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_high_low_wc[record]=line.toInt();
                record++;
            }
            record=0;
        }
        file.close();
    }
}

//Carga los valores de normalizacion
void jptwatercut::read_txt_delta_normalization(){
    QString path("");
    if(windows){
        path = "C:/JPT/External/delta_n.txt";
    }else{
        path = "/jpt/jptwatercut/External/delta_n.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de delta_n...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_delta_normalization[record]=line;
                vector_line_edit_norm[record]->setText(line);
                record++;
            }
            record=0;
        }
        file.close();
    }
}

//Guarda o actualiza los valores de normalizacion
void jptwatercut::save_txt_delta_normalization(){
    //Create route where this located the file
    QString path("");
    if(windows){
        path = "C:/JPT/External/delta_n.txt";
    }else{
        path = "/jpt/jptwatercut/External/delta_n.txt";
    }
    //Evaluate if exist file
    QFile file(path);
    //Apply in the conditional
    if(!file.exists()){
        //If not exist file, create one
        QFile create_file(path);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            //How is the first time
            //qDebug()<<"PRIMERA VEZ CONFIG";
            QTextStream input_data(&create_file);
            //Write data into file after created
            //primero va de resistividad 1 al 6 y luego de cap 1 al 6
            for(int i=0;i<vector_line_edit_norm.size();i++){
                if(vector_line_edit_norm[i]->text() != ""){
                    input_data<<vector_line_edit_norm[i]->text()<<endl;
                }else{
                    input_data<<0<<endl;
                }
            }
            //Close file
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){
            QTextStream datosArchivo(&file);
            //datosArchivo <<"";
            for(int i=0;i<vector_line_edit_norm.size();i++){
                if(vector_line_edit_norm[i]->text() != ""){
                    datosArchivo<<vector_line_edit_norm[i]->text()<<endl;
                }else{
                    datosArchivo<<0<<endl;
                }
            }
        }
        file.close();
    }
    for(int i=0;i<vector_line_edit_norm.size();i++){
        vector_delta_normalization[i] = vector_line_edit_norm[i]->text();
    }
    normalization_data();
}

//Carga los valores inversos
void jptwatercut::read_txt_inverse(){
    QString path("");

    if(windows){
        path = "C:/JPT/External/inverse.txt";
    }else {
        path = "/jpt/jptwatercut/External/inverse.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de inverse...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_inverse = line.toInt();
            }
        }
        file.close();
    }
}

//Carga los valores inversos
void jptwatercut::read_txt_inverse_values(){
    QString path("");

    if(windows){
        path = "C:/JPT/External/inverse_values.txt";
    }else{
        path = "/jpt/jptwatercut/External/inverse_values.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de inverse_values...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_inverse_values[record] = line.toInt();
                record++;
            }
        }
        file.close();
    }
}

//Carga los valores de correcion por salinidad
void jptwatercut::read_txt_correct_sali(){
    QString path("");

    if(windows){
        path = "C:/JPT/External/sali.txt";
    }else{
        path = "/jpt/jptwatercut/External/sali.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de sali...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_sali = line.toInt();
            }
        }
        file.close();
    }
}

//Carga los valores de correcion por temperatura
void jptwatercut::read_txt_correct_temp(){
    QString path("");

    if(windows){
        path = "C:/JPT/External/temp.txt";
    }else{
        path = "/jpt/jptwatercut/External/sali.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de temp...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_temp = line.toInt();
            }
        }
        file.close();
    }
}

//Carga los valores de correccion
void jptwatercut::read_txt_correct_tempval(){
    QString path("");

    if(windows){
        path = "C:/JPT/External/tempval.txt";
    }else{
        path = "/jpt/jptwatercut/External/temp.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de tempval...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<2){
                line = stream.readLine();
                if(record == 0){
                    on_off_tempval = line.toInt();
                }else{
                    value_tempval = line.toFloat();
                }
            }
            record++;
        }
        file.close();
    }
}

//Carga los valores de calibracion
void jptwatercut::read_txt_values_calibration(){
    QString path("");
    if(windows){
        path = "C:/JPT/External/calibration_values.txt";
    }else{
        path = "/jpt/jptwatercut/External/calibration_values.txt";
    }

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de calibracion...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<8){
                line = stream.readLine();
                vector_values_calibration[record]=line.toInt();
                record++;
            }
            record=0;
        }
        file.close();
    }
}

//Abre el dialogo para seleccionar la base de datos
void jptwatercut::select_db(){
    QString file_name=QFileDialog::getOpenFileName(this,"Open a DB","/home/");

    //validamos el path seleccionado y mostramos en pantalla
    if(file_name!=""){
        ui->line_path->setText(file_name);
    }
}

//Inicia el testeo y realiza las consultas necesarias para obtener los valores
void jptwatercut::test_db(){
    QString date_init = QDateTime(this->_init).toString("yyyy/MM/dd hh:mm:ss");
    QString date_end = QDateTime(this->_end).toString("yyyy/MM/dd hh:mm:ss");

    database->startDate = date_init;
    database->endDate = date_end;

    if(ui->line_path->text()!=""){
        //emit sendMsg("Connecting Database");
        database->init_database_connection(ui->line_path->text());
        //Si lee correctamente la BD generamos la obtencion de la data
        try {
            //Seteamos el tiempo que queremos los datos
            //database->charge_data_on_memory(date_init, date_end);
            QStringList split = ui->line_path->text().split(".");
            if(split[1]== "db"){
                database->start();
            }
        } catch (...) {
            //emit sendMsg("The system couldn't charge the database info");
        }
    }else{
        //emit sendMsg("Database Error: Direction Error.");
        //emit sendMsg("Please, try again");
    }
}

//Termina el hilo que se ejecuta para leer la base de datos
void jptwatercut::kill_thread(){
    database->terminate();
    //Lo primero es generar la tabla de los datos RAW
    charge_table_old();
}


void jptwatercut::active_thread_temp(){
    //se termina el hilo donde ejecuta la normalizacion
}

void jptwatercut::active_thread_salinity(){

}

//Genera la tabla de la data raw y escoge el numero minimo para reccorrer las listas
//Luego genera el plot con respecto a la data raw
void jptwatercut::charge_table_old(){
    clean_all();
    /*
    while (ui->table_old_db->rowCount() > 0){
        ui->table_old_db->removeRow(0);
    }
    */

    QList<int> values;
    for (int i=0;i<database->get_data_all_nodes_old().count();i++) {
        values.append(database->get_data_all_nodes_old()[i].count());
    }
    min = *std::min_element(values.begin(), values.end());
    qDebug()<<"min ->"<<min;
    //Init table con el modelo de la tabla, (rows, columns, object)
    model_old = new QStandardItemModel(min, 14, this);
    //ui->table_old_db->setColumnCount(14);
    QStringList headers;
    //Sets headers in tables
    headers << "RES-1" << "CAP-1" << "RES-2"  << "CAP-2" << "RES-3" << "CAP-3" << "RES-4"<< "CAP-4"<< "RES-5"<< "CAP-5"<< "RES-6"<< "CAP-6"<< "TEMP-RAW"<< "TEMP-ENG";
    model_old->setHorizontalHeaderLabels(headers);

    ui->table_old_db->setModel(model_old);
    int fila=0;
    //creo las posiciones necesarias para las compensaciones para que asi no se salgo por error
    fill_all();

    for (int j=0;j<min;j++){
        for(int col = 0; col < 14; col++){
            QModelIndex index = model_old->index(j,col,QModelIndex());
            // 0 for all data
            if((col % 2) == 0){
                //resistivos
                model_old->setData(index, QString::number(database->get_data_all_nodes_old()[fila][j].get_raw_resis()));
            }else{
                model_old->setData(index, QString::number(database->get_data_all_nodes_old()[fila][j].get_eng_capa()));
                fila++;
                if(fila > 6){
                    fila=0;
                }
            }

            if(col == 12){
                model_old->setData(index, QString::number(database->get_data_all_nodes_old()[6][j].get_raw_tempe()));
            }else if(col == 13){
                model_old->setData(index, QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe()));
            }
        }
    }

    //Generate table normalization

    //Luego dentro de aqui, se genera una señal la cual me indica que ya termino y que se puede usar calcular la normalizacion.

    //plot de raw
    plot_raw_data();
}

//Carga la informacion para enviarla a otra clase para calcular la normalizacion
void jptwatercut::charge_norm_data(){
    try {
        normalization->database = database;
        normalization->vector_delta_normalization.clear();
        for(int i=0;i<12;i++){
            normalization->vector_delta_normalization.append(vector_delta_normalization[i]);
        }
        normalization->min = min;
        if(ui->check_norm_data->isChecked()){
            normalization->select = true;
        }else{
            normalization->select = false;
        }
        normalization->start();
    } catch (...) {

    }
}

//Obtiene todos los valores de la normalizacion
void jptwatercut::get_norm(){
    _node_norm = normalization->get_data_norm();

    qDebug()<<"llegoooooo normalizacion";

    normalization->terminate();
    normalization_data();
}

//Genera la tabla de la data normalizacion y escoge el numero minimo para reccorrer las listas
//Luego genera el plot con respecto a la data normalizacion
void jptwatercut::normalization_data(){
    qDebug()<<"norm";

    if(ui->check_norm_data->isChecked()){
        //Init table
        //ui->table_norma_data->setColumnCount(12);
        QStringList headers;
        //Sets headers in tables
        headers << "RES-1" << "CAP-1" << "RES-2"  << "CAP-2" << "RES-3" << "CAP-3" << "RES-4"<< "CAP-4"<< "RES-5"<< "CAP-5"<< "RES-6"<< "CAP-6";
        //ui->table_norma_data->setHorizontalHeaderLabels(headers);
        model_norm = new QStandardItemModel(min, 12, this);
        model_norm->setHorizontalHeaderLabels(headers);

        ui->table_norma_data->setModel(model_norm);
        int fila=0;
        QList<int> gene;
        /*
        for (int j=0;j<min;j++){
            for(int col = 0; col < 12; col++){
                QModelIndex index = model_norm->index(j,col,QModelIndex());
                // 0 for all data
                if((col % 2) == 0){
                    //resistivos
                    //qDebug()<<"fila "<<fila<<" "<<_node_norm[j][fila].get_res_norm();
                    model_norm->setData(index, QString::number(_node_norm[j][fila].get_res_norm()));

                    qDebug()<<"resssssssss "<<QString::number(_node_norm[j][fila].get_res_norm());
                    //model_norm->setData(index, QString::number(database->get_data_all_nodes_old()[fila][j].get_raw_resis()));
                }else{
                    model_norm->setData(index, QString::number(_node_norm[j][fila].get_cap_norm()));

                    qDebug()<<"cappppppppp "<<QString::number(_node_norm[j][fila].get_cap_norm());
                    //model_norm->setData(index, QString::number(database->get_data_all_nodes_old()[fila][j].get_raw_capa()));
                    fila++;
                    if(fila > 5){
                        fila=0;
                    }
                }
            }

        }
        */
    }
    plot_norm_data();
    //se plotea la normalizacion
}

//Genera una compensacion por temperatura al valor normalizado (AUN NO SE USA)
//Luego genera la compensacion por salinidad
void jptwatercut::compensation_temperature(){
    int raw_cap, raw_resis, comp_resis, comp_capa, temp_value_res=0, temp_value_capa=0;
    QList<int> list_raw, list_cap;
    if(ui->check_com_temp->isChecked()){
        //se limpia en caso de que se haga la compensacion
        vector_res_temp.clear();
        vector_capa_temp.clear();
    }

    //esta es una variable temporal que saca el avg de la temperatura
    float avg_temp=0;

    for (int i=0;i<min;i++) {
        list_cap.clear();
        list_raw.clear();
        for (int j=0;j<6;j++) {
            //Aqui se calcula temperatura eng
            //RECORDAR QUE POR AHORA TENEMOS VALORES DE ENG DESDE LA BD
            //CUANDO SE HAGA LA OTRA ADAPTACION PROBAMOS CON LA ENTRADA DE CADA UNA
            //vector_abs_sensors[i]->calculate_physic_temp(0, database->)

            //Variables temporales raw
            raw_resis = _node_norm[i][j].get_res_norm();
            raw_cap = _node_norm[i][j].get_cap_norm();

            if(ui->check_com_temp->isChecked()){
                //En caso que solo sea por parte de lo que llega
                float eng_temp = database->get_data_all_nodes_old()[6][i].get_eng_tempe();
                if(ui->check_user_temp->isChecked()){
                    //Aqui es cuando compenso con el valor del spin
                    temp_value_res = (res_sensors->calcule_cuentas_temp(int(ui->spin_comp_temp->value())));
                    temp_value_capa = (cap_sensors->calcule_cuentas_temp(int(ui->spin_comp_temp->value())));

                    list_raw.append(temp_value_res);
                    list_cap.append(temp_value_capa);
                    //vector_res_temp[i][j] = raw_resis + temp_value_res;
                    //vector_capa_temp[i][j] = raw_cap + temp_value_capa;
                }else{
                    temp_value_res = (res_sensors->calcule_cuentas_temp(eng_temp));
                    temp_value_capa = (cap_sensors->calcule_cuentas_temp(eng_temp));

                    list_raw.append(temp_value_res);
                    list_cap.append(temp_value_capa);
                    //vector_res_temp[i][j] = raw_resis + temp_value_res;
                    //vector_capa_temp[i][j] = raw_cap + temp_value_capa;
                }
            }
        }
        vector_res_temp.append(list_raw);
        vector_capa_temp.append(list_cap);

        //acumulamos la temperatura
        avg_temp+=database->get_data_all_nodes_old()[6][i].get_eng_tempe();
    }
    ui->line_result_temp->setText(QString::number((avg_temp/min), 'f', 2)+" °C");

    compensation_salinity();
}

//Calcula la salinidad en el agua por medio de los sensores res1 y res6
//En caso de que se requiera se usa la compesacion por salinidad (AUN NO SE USA)
//Genera el plot de salinidad
void jptwatercut::compensation_salinity(){
    qDebug()<<"entro salinidad";
    int raw_resis_r1, raw_resis_r6, comp_resis_r1, comp_resis_r6, comp_resis, comp_capa;
    QList<int> list_res, list_cap;

    //variable acumulativa de salinidad
    float sali =0;

    //Se revisa si realmente se va a usar la compensacion por salinidad
    if(ui->check_com_sal->isChecked()){
        vector_res_salinity.clear();
        vector_capa_salinity.clear();
        //ahora se calcula las cuentas de salinidad
        for (int i=0;i<min;i++) {
            list_res.clear();
            list_cap.clear();
            for (int j=0;j<6;j++) {
                comp_resis = res_sensors->calcule_delta_sali(vector_real_salinity[i]);
                list_res.append(comp_resis);

                comp_capa = cap_sensors->calcule_delta_sali(vector_real_salinity[i]);
                list_cap.append(comp_capa);
            }
            vector_res_salinity.append(list_res);
            vector_capa_salinity.append(list_cap);
        }
    }else if(ui->check_user_sali->isChecked()){
        vector_res_salinity.clear();
        vector_capa_salinity.clear();
        for (int i=0;i<min;i++) {
            list_res.clear();
            list_cap.clear();
            for (int j=0;j<6;j++) {
                comp_resis = res_sensors->calcule_delta_sali(ui->spin_comp_sali->value());
                list_res.append(comp_resis);

                comp_capa = cap_sensors->calcule_delta_sali(ui->spin_comp_sali->value());
                list_cap.append(comp_capa);
            }
            vector_res_salinity.append(list_res);
            vector_capa_salinity.append(list_cap);
        }
    }else{
        for (int i=0;i<min;i++) {
            //Variables temporales raw

            raw_resis_r1 = _node_norm[i][0].get_res_norm();
            raw_resis_r6 = _node_norm[i][5].get_res_norm();

            //Condicional en caso de los dos sensores resistivos 1,6
            if(ui->check_com_sal_r1->isChecked() && ui->check_com_sal_r6->isChecked()){
                //En caso que solo sea por parte de lo que llega
                //salinity
                comp_resis_r1 = res_sensors->calcule_new_salinity(raw_resis_r1);
                comp_resis_r6 = res_sensors->calcule_new_salinity(raw_resis_r6);

                //No pueden ser valores negativos

                if(comp_resis_r1 < 0){
                    //comp_resis_r1 = 0;
                    comp_resis_r1=abs(comp_resis_r1);
                }
                if(comp_resis_r6 < 0){
                    //comp_resis_r6 = 0;
                    comp_resis_r6=abs(comp_resis_r1);
                }
                //qDebug()<<"Salinidad R1 "<<comp_resis_r1;
                //qDebug()<<"Salinidad R6 "<<comp_resis_r6;
                int avg = (comp_resis_r1+comp_resis_r6)/2;
                vector_real_salinity.append(avg);
            }else if(ui->check_com_sal_r1->isChecked() && !ui->check_com_sal_r6->isChecked()){
                //Condicional en caso de los dos sensores resistivo 1
                //En caso que solo sea por parte de lo que llega
                //salinity
                comp_resis_r1 = res_sensors->calcule_new_salinity(raw_resis_r1);

                //No pueden ser valores negativos
                if(comp_resis_r1 < 0){
                    //comp_resis_r1 = 0;
                    comp_resis_r1=abs(comp_resis_r1);
                }
                //qDebug()<<"Salinidad R1 "<<comp_resis_r1;
                vector_real_salinity.append(comp_resis_r1);
            }else if(!ui->check_com_sal_r1->isChecked() && ui->check_com_sal_r6->isChecked()){
                //Condicional en caso de los dos sensores resistivo 1
                //En caso que solo sea por parte de lo que llega
                //salinity
                comp_resis_r6 = res_sensors->calcule_new_salinity(raw_resis_r6);
                //No pueden ser valores negativos
                if(comp_resis_r6 < 0){
                    //comp_resis_r6 = 0;
                    comp_resis_r6=abs(comp_resis_r6);
                }
                //qDebug()<<"Salinidad R6 "<<comp_resis_r6;
                vector_real_salinity.append(comp_resis_r6);
            }else{
                vector_real_salinity.append(0);
            }

            sali += vector_real_salinity[i];
        }
    }

    //seteamos el promedio de salinidad
    ui->line_result_sali->setText(QString::number((sali/min),'f',2) + " PPM");

    //ploteo de salinidad
    plot_salinity();
}

//Permite la funcionalidad de zoom y movimiento en el plot de raw
void jptwatercut::mouseWheel_raw(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_data_raw->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_raw->axisRect()->setRangeZoomAxes(ui->chart_data_raw->xAxis,ui->chart_data_raw->yAxis);
        ui->chart_data_raw->axisRect()->setRangeZoom(ui->chart_data_raw->xAxis->orientation());
    }
    else if (ui->chart_data_raw->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_raw->axisRect()->setRangeZoomAxes(ui->chart_data_raw->xAxis,ui->chart_data_raw->yAxis);
        ui->chart_data_raw->axisRect()->setRangeZoom(ui->chart_data_raw->yAxis->orientation());
    }
    else if (ui->chart_data_raw->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_raw->axisRect()->setRangeZoomAxes(ui->chart_data_raw->xAxis2,ui->chart_data_raw->yAxis2);
        ui->chart_data_raw->axisRect()->setRangeZoom(ui->chart_data_raw->xAxis2->orientation());
    }
    else if (ui->chart_data_raw->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_raw->axisRect()->setRangeZoomAxes(ui->chart_data_raw->xAxis2,ui->chart_data_raw->yAxis2);
        ui->chart_data_raw->axisRect()->setRangeZoom(ui->chart_data_raw->yAxis2->orientation());
    }
    else
        ui->chart_data_raw->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_raw(QMouseEvent *event){

    int x = ui->chart_data_raw->xAxis->pixelToCoord(event->pos().x());
    int y = ui->chart_data_raw->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("VectorX %1 ,WC %2").arg(x).arg(y));
}

//Permite la funcionalidad de zoom y movimiento en el plot de normalizacion
void jptwatercut::mouseWheel_norm(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_data_norm->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_norm->axisRect()->setRangeZoomAxes(ui->chart_data_norm->xAxis,ui->chart_data_norm->yAxis);
        ui->chart_data_norm->axisRect()->setRangeZoom(ui->chart_data_norm->xAxis->orientation());
    }
    else if (ui->chart_data_norm->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_norm->axisRect()->setRangeZoomAxes(ui->chart_data_norm->xAxis,ui->chart_data_norm->yAxis);
        ui->chart_data_norm->axisRect()->setRangeZoom(ui->chart_data_norm->yAxis->orientation());
    }
    else if (ui->chart_data_norm->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_norm->axisRect()->setRangeZoomAxes(ui->chart_data_norm->xAxis2,ui->chart_data_norm->yAxis2);
        ui->chart_data_norm->axisRect()->setRangeZoom(ui->chart_data_norm->xAxis2->orientation());
    }
    else if (ui->chart_data_norm->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_norm->axisRect()->setRangeZoomAxes(ui->chart_data_norm->xAxis2,ui->chart_data_norm->yAxis2);
        ui->chart_data_norm->axisRect()->setRangeZoom(ui->chart_data_norm->yAxis2->orientation());
    }
    else
        ui->chart_data_norm->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_norm(QMouseEvent *event){

    int x = ui->chart_data_norm->xAxis->pixelToCoord(event->pos().x());
    int y = ui->chart_data_norm->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("VectorX %1 ,WC %2").arg(x).arg(y));
}


//Permite la funcionalidad de zoom y movimiento en el plot de salinidad
void jptwatercut::mouseWheel_salinity(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_data_sal->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_sal->axisRect()->setRangeZoomAxes(ui->chart_data_sal->xAxis,ui->chart_data_sal->yAxis);
        ui->chart_data_sal->axisRect()->setRangeZoom(ui->chart_data_sal->xAxis->orientation());
    }
    else if (ui->chart_data_sal->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_sal->axisRect()->setRangeZoomAxes(ui->chart_data_sal->xAxis,ui->chart_data_sal->yAxis);
        ui->chart_data_sal->axisRect()->setRangeZoom(ui->chart_data_sal->yAxis->orientation());
    }
    else if (ui->chart_data_sal->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_sal->axisRect()->setRangeZoomAxes(ui->chart_data_sal->xAxis2,ui->chart_data_sal->yAxis2);
        ui->chart_data_sal->axisRect()->setRangeZoom(ui->chart_data_sal->xAxis2->orientation());
    }
    else if (ui->chart_data_sal->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_data_sal->axisRect()->setRangeZoomAxes(ui->chart_data_sal->xAxis2,ui->chart_data_sal->yAxis2);
        ui->chart_data_sal->axisRect()->setRangeZoom(ui->chart_data_sal->yAxis2->orientation());
    }
    else
        ui->chart_data_sal->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_salinity(QMouseEvent *event){
    int x = ui->chart_data_sal->xAxis->pixelToCoord(event->pos().x());
    int y = ui->chart_data_sal->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("VectorX %1 ,WC %2").arg(x).arg(y));
}

//Genera el plot de la data raw
void jptwatercut::plot_raw_data(){
    ui->chart_data_raw->clearGraphs();
    ui->chart_data_raw->clearItems();
    ui->chart_data_raw->clearPlottables();

    QVector<QCPGraphData> timeData(250);

    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_data_raw, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_raw(QMouseEvent*)));

    connect(ui->chart_data_raw, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_raw()));

    //tengo los dos vectores de corte resistivo y capacitivo
    //instancia que crea los tickers para las fechas
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    ui->chart_data_raw->addGraph();
    //r1
    ui->chart_data_raw->graph(0)->setPen(QPen(QColor(252,90,3)));
    ui->chart_data_raw->graph(0)->setName("Resistivity 1");
    //c1
    ui->chart_data_raw->graph(1)->setPen(QPen(QColor(196,69,0)));
    ui->chart_data_raw->graph(1)->setName("Capacitance 1");
    //r2
    ui->chart_data_raw->graph(2)->setPen(QPen(QColor(105, 105, 105)));
    ui->chart_data_raw->graph(2)->setName("Resistivity 2");
    //c2
    ui->chart_data_raw->graph(3)->setPen(QPen(QColor(64, 64, 64)));
    ui->chart_data_raw->graph(3)->setName("Capacitance 2");
    //r3
    ui->chart_data_raw->graph(4)->setPen(QPen(QColor(48, 56, 227)));
    ui->chart_data_raw->graph(4)->setName("Resistivity 3");
    //c3
    ui->chart_data_raw->graph(5)->setPen(QPen(QColor(0, 7, 166)));
    ui->chart_data_raw->graph(5)->setName("Capacitance 3");
    //r4
    ui->chart_data_raw->graph(6)->setPen(QPen(QColor(44, 219, 105)));
    ui->chart_data_raw->graph(6)->setName("Resistivity 4");
    //c4
    ui->chart_data_raw->graph(7)->setPen(QPen(QColor(0, 153, 53)));
    ui->chart_data_raw->graph(7)->setName("Capacitance 4");
    //r5
    ui->chart_data_raw->graph(8)->setPen(QPen(QColor(143, 89, 63)));
    ui->chart_data_raw->graph(8)->setName("Resistivity 5");
    //c5
    ui->chart_data_raw->graph(9)->setPen(QPen(QColor(107, 36, 1)));
    ui->chart_data_raw->graph(9)->setName("Capacitance 5");
    //r6
    ui->chart_data_raw->graph(10)->setPen(QPen(QColor(197, 204, 69)));
    ui->chart_data_raw->graph(10)->setName("Resistivity 6");
    //c6
    ui->chart_data_raw->graph(11)->setPen(QPen(QColor(215, 227, 0)));
    ui->chart_data_raw->graph(11)->setName("Capacitance 6");

    QVector<double> x(min), y(min), y2(min), y3(min), y4(min), y5(min), y6(min),
            y7(min), y8(min), y9(min), y10(min), y11(min), y12(min);

    int counter=0;

    for (int i=0;i<min;i++) {
        x[i]= i*100;

        y[i] = database->get_data_all_nodes_old()[0][i].get_raw_resis();
        y2[i] = database->get_data_all_nodes_old()[0][i].get_raw_capa();
        y3[i] = database->get_data_all_nodes_old()[1][i].get_raw_resis();
        y4[i] = database->get_data_all_nodes_old()[1][i].get_raw_capa();
        y5[i] = database->get_data_all_nodes_old()[2][i].get_raw_resis();
        y6[i] = database->get_data_all_nodes_old()[2][i].get_raw_capa();
        y7[i] = database->get_data_all_nodes_old()[3][i].get_raw_resis();
        y8[i] = database->get_data_all_nodes_old()[3][i].get_raw_capa();
        y9[i] = database->get_data_all_nodes_old()[4][i].get_raw_resis();
        y10[i] = database->get_data_all_nodes_old()[4][i].get_raw_capa();
        y11[i] = database->get_data_all_nodes_old()[5][i].get_raw_resis();
        y12[i] = database->get_data_all_nodes_old()[5][i].get_raw_capa();

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];

        if(i==0){
            textTicker->addTick(x[i], date);
        }

        if(counter==10){
            textTicker->addTick(x[i], date);
            counter =0;
        }
    }

    // configure bottom axis to show date instead of number:
    ui->chart_data_raw->xAxis->setTicker(textTicker);

    ui->chart_data_raw->graph(0)->setData(x, y);
    ui->chart_data_raw->graph(1)->setData(x, y2);
    ui->chart_data_raw->graph(2)->setData(x, y3);
    ui->chart_data_raw->graph(3)->setData(x, y4);
    ui->chart_data_raw->graph(4)->setData(x, y5);
    ui->chart_data_raw->graph(5)->setData(x, y6);
    ui->chart_data_raw->graph(6)->setData(x, y7);
    ui->chart_data_raw->graph(7)->setData(x, y8);
    ui->chart_data_raw->graph(8)->setData(x, y9);
    ui->chart_data_raw->graph(9)->setData(x, y10);
    ui->chart_data_raw->graph(10)->setData(x, y11);
    ui->chart_data_raw->graph(11)->setData(x, y12);

    ui->chart_data_raw->graph(0)->rescaleAxes();
    ui->chart_data_raw->graph(1)->rescaleAxes(true);

    ui->chart_data_raw->xAxis->setLabel("Date");
    ui->chart_data_raw->yAxis->setLabel("Resistivity & Capacitance");

    //label que muestra el chart
    ui->chart_data_raw->legend->setVisible(true);
    // set axes ranges, so we see all data:
    ui->chart_data_raw->xAxis->setRange(-1, min);
    ui->chart_data_raw->yAxis->setRange(0, min);
    ui->chart_data_raw->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_data_raw->replot();

    charge_norm_data();
}

//Genera el plot de la data normalizada
void jptwatercut::plot_norm_data(){
    ui->chart_data_norm->clearGraphs();
    ui->chart_data_norm->clearItems();
    ui->chart_data_norm->clearPlottables();

    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_data_norm, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_norm(QMouseEvent*)));

    connect(ui->chart_data_norm, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_norm()));

    //tengo los dos vectores de corte resistivo y capacitivo
    //instancia que crea los tickers para las fechas
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    ui->chart_data_norm->addGraph();
    //r1
    ui->chart_data_norm->graph(0)->setPen(QPen(QColor(252,90,3)));
    ui->chart_data_norm->graph(0)->setName("Resistivity 1");
    //c1
    ui->chart_data_norm->graph(1)->setPen(QPen(QColor(196,69,0)));
    ui->chart_data_norm->graph(1)->setName("Capacitance 1");
    //r2
    ui->chart_data_norm->graph(2)->setPen(QPen(QColor(105, 105, 105)));
    ui->chart_data_norm->graph(2)->setName("Resistivity 2");
    //c2
    ui->chart_data_norm->graph(3)->setPen(QPen(QColor(64, 64, 64)));
    ui->chart_data_norm->graph(3)->setName("Capacitance 2");
    //r3
    ui->chart_data_norm->graph(4)->setPen(QPen(QColor(48, 56, 227)));
    ui->chart_data_norm->graph(4)->setName("Resistivity 3");
    //c3
    ui->chart_data_norm->graph(5)->setPen(QPen(QColor(0, 7, 166)));
    ui->chart_data_norm->graph(5)->setName("Capacitance 3");
    //r4
    ui->chart_data_norm->graph(6)->setPen(QPen(QColor(44, 219, 105)));
    ui->chart_data_norm->graph(6)->setName("Resistivity 4");
    //c4
    ui->chart_data_norm->graph(7)->setPen(QPen(QColor(0, 153, 53)));
    ui->chart_data_norm->graph(7)->setName("Capacitance 4");
    //r5
    ui->chart_data_norm->graph(8)->setPen(QPen(QColor(143, 89, 63)));
    ui->chart_data_norm->graph(8)->setName("Resistivity 5");
    //c5
    ui->chart_data_norm->graph(9)->setPen(QPen(QColor(107, 36, 1)));
    ui->chart_data_norm->graph(9)->setName("Capacitance 5");
    //r6
    ui->chart_data_norm->graph(10)->setPen(QPen(QColor(197, 204, 69)));
    ui->chart_data_norm->graph(10)->setName("Resistivity 6");
    //c6
    ui->chart_data_norm->graph(11)->setPen(QPen(QColor(215, 227, 0)));
    ui->chart_data_norm->graph(11)->setName("Capacitance 6");

    QVector<double> x(min), y(min), y2(min), y3(min), y4(min), y5(min), y6(min),
            y7(min), y8(min), y9(min), y10(min), y11(min), y12(min);

    int counter=0;

    for (int i=0;i<min;i++) {

        x[i]= i*100;
        //x[i]= i*100;
        y[i] = _node_norm[i][0].get_res_norm();
        y2[i] = _node_norm[i][0].get_cap_norm();
        y3[i] = _node_norm[i][1].get_res_norm();
        y4[i] = _node_norm[i][1].get_cap_norm();
        y5[i] = _node_norm[i][2].get_res_norm();
        y6[i] = _node_norm[i][2].get_cap_norm();
        y7[i] = _node_norm[i][3].get_res_norm();
        y8[i] = _node_norm[i][3].get_cap_norm();
        y9[i] = _node_norm[i][4].get_res_norm();
        y10[i] = _node_norm[i][4].get_cap_norm();
        y11[i] = _node_norm[i][5].get_res_norm();
        y12[i] = _node_norm[i][5].get_cap_norm();

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];


        if(i==0){
            textTicker->addTick(x[i], date);
        }

        if(counter==10){
            textTicker->addTick(x[i], date);
            counter =0;
        }
    }

    ui->chart_data_norm->xAxis->setTicker(textTicker);

    ui->chart_data_norm->graph(0)->setData(x, y);
    ui->chart_data_norm->graph(1)->setData(x, y2);
    ui->chart_data_norm->graph(2)->setData(x, y3);
    ui->chart_data_norm->graph(3)->setData(x, y4);
    ui->chart_data_norm->graph(4)->setData(x, y5);
    ui->chart_data_norm->graph(5)->setData(x, y6);
    ui->chart_data_norm->graph(6)->setData(x, y7);
    ui->chart_data_norm->graph(7)->setData(x, y8);
    ui->chart_data_norm->graph(8)->setData(x, y9);
    ui->chart_data_norm->graph(9)->setData(x, y10);
    ui->chart_data_norm->graph(10)->setData(x, y11);
    ui->chart_data_norm->graph(11)->setData(x, y12);

    ui->chart_data_norm->graph(0)->rescaleAxes();
    ui->chart_data_norm->graph(1)->rescaleAxes(true);


    ui->chart_data_norm->xAxis->setLabel("Date");
    ui->chart_data_norm->yAxis->setLabel("Resistivity & Capacitance");

    //label que muestra el chart
    ui->chart_data_norm->legend->setVisible(true);
    // set axes ranges, so we see all data:
    ui->chart_data_norm->xAxis->setRange(-1, min);
    ui->chart_data_norm->yAxis->setRange(0, min);
    ui->chart_data_norm->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_data_norm->replot();

    //calcule the temperature average
    compensation_temperature();
}

//Genera el plot de los valores de salinidad
void jptwatercut::plot_salinity(){
    ui->chart_data_sal->clearGraphs();
    ui->chart_data_sal->clearItems();
    ui->chart_data_sal->clearPlottables();

    int flag=10;
    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_data_sal, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_salinity(QMouseEvent*)));

    connect(ui->chart_data_sal, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_salinity()));

    //tengo los dos vectores de corte resistivo y capacitivo
    ui->chart_data_sal->addGraph();

    ui->chart_data_sal->graph(0)->setPen(QPen(Qt::blue));
    ui->chart_data_sal->graph(0)->setName("Salinity PPM");

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    QVector<double> x(min), y(min);

    int counter=0;

    for (int i=0;i<min;i++) {
        x[i]= i*100;
        y[i] = vector_real_salinity[i];

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];


        if(i==0){
            textTicker->addTick(x[i], date);
        }

        if(counter==10){
            textTicker->addTick(x[i], date);
            counter =0;
        }

    }

    ui->chart_data_sal->xAxis->setTicker(textTicker);
    ui->chart_data_sal->graph(0)->setData(x, y);

    ui->chart_data_sal->xAxis->setLabel("Date");
    ui->chart_data_sal->yAxis->setLabel("Salinity");
    // set axes ranges, so we see all data:

    ui->chart_data_sal->legend->setVisible(true);

    ui->chart_data_sal->xAxis->setRange(-1, min);
    ui->chart_data_sal->yAxis->setRange(0, min);
    ui->chart_data_sal->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_data_sal->replot();

    model_percent();
}

//Por medio de unos calculos genera unos porcentajes de aproximacion
//para tener una idea de a que modelo corresponde la data que se esta leyendo.
void jptwatercut::model_percent(){

    int condi = ui->spin_value_th_oil_lei->value(), diff =ui->spin_value_th_diff_lei->value();

    int spin_flow = int(ui->spin_value_cps_tur->value());

    QList<int> list_res, list_cap;

    float counter_true_lam =0, counter_true_lei=0, counter_true_turb=0, counter_true_turb_lam=0;

    for (int j=0;j<min;j++) {
        int temp_resis1 =0, temp_resis2 =0, temp_resis3 =0, temp_resis4 =0, temp_resis5 =0,
                temp_resis6 =0, temp_capa1=0, temp_capa2 =0, temp_capa3 =0, temp_capa4 =0,
                temp_capa5 =0, temp_capa6=0;

        list_cap.clear();
        list_res.clear();

        temp_resis1 = _node_norm[j][0].get_res_norm() ;
        temp_capa1 = _node_norm[j][0].get_cap_norm() ;

        temp_resis2 = _node_norm[j][1].get_res_norm() ;
        temp_capa2 = _node_norm[j][1].get_cap_norm() ;

        temp_resis3 = _node_norm[j][2].get_res_norm();
        temp_capa3 = _node_norm[j][2].get_cap_norm() ;

        temp_resis4 = _node_norm[j][3].get_res_norm() ;
        temp_capa4 = _node_norm[j][3].get_cap_norm() ;

        temp_resis5 = _node_norm[j][4].get_res_norm() ;
        temp_capa5 = _node_norm[j][4].get_cap_norm();

        temp_resis6 = _node_norm[j][5].get_res_norm() ;
        temp_capa6 = _node_norm[j][5].get_cap_norm() ;

        //EVALUACION PARA LAMINAR
        if(temp_resis1<ui->spin_value_th_water_lam_resis->value() && temp_resis4>ui->spin_value_th_oil_lam_resis->value() &&
                temp_capa1<ui->spin_value_th_water_lam_capa->value() && temp_capa4>ui->spin_value_th_oil_lam_capa->value()){
            //qDebug()<<"TRUE LAMINAR";
            counter_true_lam++;
        }

        //EVALUACION PARA LAMINAR EI
        if(temp_resis1>condi){
            if(temp_capa1<temp_resis1 && (temp_resis1-temp_capa1)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis2>condi){
            if(temp_capa2<temp_resis2 && (temp_resis2-temp_capa2)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis3>condi){
            if(temp_capa3<temp_resis3 && (temp_resis3-temp_capa3)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis4>condi){
            if(temp_capa4<temp_resis4 && (temp_resis4-temp_capa4)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis5>condi){
            if(temp_capa5<temp_resis5 && (temp_resis5-temp_capa5)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis6>condi){
            if(temp_capa6<temp_resis6 && (temp_resis6-temp_capa6)>diff){
                counter_true_lei+=0.16667;
            }
        }

        //se llenan las listas resistivas
        list_res.append(temp_resis1);
        list_res.append(temp_resis2);
        list_res.append(temp_resis3);
        list_res.append(temp_resis4);
        list_res.append(temp_resis5);
        list_res.append(temp_resis6);
        //se llenan las listas capacitivas
        list_cap.append(temp_capa1);
        list_cap.append(temp_capa2);
        list_cap.append(temp_capa3);
        list_cap.append(temp_capa4);
        list_cap.append(temp_capa5);
        list_cap.append(temp_capa6);

        //EVALUACION PARA TURBULENTO

        //se hace el contador con 1/60 ya que son 30 evaluaciones de resistivos y 30 capacitivos

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_res[i], list_res[j]);
                    if(value <= spin_flow){
                        counter_true_turb+=0.01666;
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_cap[i], list_cap[j]);
                    if(value <= spin_flow){
                        counter_true_turb+=0.01666;
                    }
                }
            }
        }

        //EVALUACION PARA TURBULENTO LAMINAR

        if(temp_resis1<ui->spin_value_th_water_lam_resis->value() && temp_resis4>ui->spin_value_th_oil_lam_resis->value() &&
                temp_capa1<ui->spin_value_th_water_lam_capa->value() && temp_capa4>ui->spin_value_th_oil_lam_capa->value()){

            for (int i=0;i<6;i++) {
                //qDebug()<<"value lista res "<<list_res[i];
                for (int j=0;j<6;++j) {
                    if(i!=0 && i!=3 && i!=j){
                        if(j!=0 && j!=3){
                            int value=0;
                            value = subs_values(list_res[i], list_res[j]);
                            if(value <= spin_flow){
                                counter_true_turb_lam+=0.025;
                            }
                        }
                    }
                }
            }

            for (int i=0;i<6;i++) {
                //qDebug()<<"value lista cap "<<list_cap[i];
                for (int j=0;j<6;++j) {
                    if(i!=0 && i!=3 && i!=j){
                        if(j!=0 && j!=3){
                            int value=0;
                            value = subs_values(list_cap[i], list_cap[j]);
                            if(value <= spin_flow){
                                counter_true_turb_lam+=0.025;
                            }
                        }
                    }
                }
            }

        }
    }

    //calculo para porcentaje de cumplimiento de modelo

    //LAMINAR
    float percent_true_lam = (counter_true_lam * 100)/min;

    qDebug()<<"% LM"<<percent_true_lam;

    ui->line_percent_lam->setText(QString::number(percent_true_lam,'f',2) + "%");

    //LAMINAR EI
    float percent_true_lei = (counter_true_lei*100)/min;

    qDebug()<<"% LEI "<<percent_true_lei;

    ui->line_percent_lei->setText(QString::number(percent_true_lei, 'f',2) + "%");

    //TURBULENTO
    float percent_true_turb = (counter_true_turb*100)/min;

    ui->line_percent_turb->setText(QString::number(percent_true_turb,'f',2) + "%");
    qDebug()<<"% TURB "<<percent_true_turb;

    //TURBULENTO LAM
    float percent_true_turb_lam = (counter_true_turb_lam*100)/min;
    //float percent_true_turb_ = (counter_true_turb_lam*100)/min;

    ui->line_percent_turb_lam->setText(QString::number(percent_true_turb_lam,'f',2) + "%");
    //ui->line_percent_turb_lam_2->setText(QString::number(percent_true_turb_,'f',2) + "%");

    // qDebug()<<"% LAM "<<percent_true_turb_lam;
    qDebug()<<"% TURB LAM "<<percent_true_turb_lam;
}

//Calcula el corte resistivo para el modelo laminar
void jptwatercut::watercut_resistivity(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    vector_watercut_resistivity_ei.clear();
    vector_watercut_resistivity.clear();

    QList<float> list_wc_res;
    if(on_off_inverse==1){
        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                int total_counts_res = int(vector_res_inverse_emulsion[j][i] + vector_res_temp[j][i] + vector_res_salinity[j][i]);

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }

                list_wc_res.append(watercut_resis);
            }
            vector_watercut_resistivity_ei.append(list_wc_res);
        }
    }else{

        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                total_counts_res = int(_node_norm[j][i].get_res_norm());

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }
                list_wc_res.append(watercut_resis);
                //qDebug()<<"total res lam "<<list_wc_res[i]<<" i "<<i;

            }
            vector_watercut_resistivity.append(list_wc_res);
        }
    }
}

//Calcula el corte capacitivo para el modelo laminar
void jptwatercut::watercut_capacitive(){
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    QList<float> list_wc_cap;
    vector_watercut_capacitance.clear();

    //con estos valores debo de generar mi curva dinamica.
    /*
     * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
     *
     */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;

    for (int j=0;j<min;j++) {
        list_wc_cap.clear();
        for (int i=0;i<6;i++) {
            int raw_norm_cap=0;

            raw_norm_cap = _node_norm[j][i].get_cap_norm();

            float watercut_for_sensor=0;

            if(ui->radio_hc_hf->isChecked()){
                //alto corte, alto flujo
                watercut_for_sensor = (-0.0002 * raw_norm_cap) + 2.8;
            }else if(ui->radio_lc_hf->isChecked()){
                //bajo corte, alto flujo
                watercut_for_sensor = (-0.000008 * raw_norm_cap) + 1.2385;
            }else if(ui->radio_hc_lf->isChecked()){
                //alto corte, bajo flujo
                if(i == 3){
                    QScriptEngine engine_functions_cap_4;
                    QScriptValue master_value_cap_4;

                    //solo aplica para el capacitivo del nodo 4
                    QString replace_cap_4 = ui->line_hc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_4 = engine_functions_cap_4.evaluate(replace_cap_4);

                    watercut_for_sensor = master_value_cap_4.toNumber();
                }

            }else if(ui->radio_lc_lf->isChecked()){
                //bajo corte, bajo flujo
                if(i == 0){
                    QScriptEngine engine_functions_cap_1;
                    QScriptValue master_value_cap_1;

                    //solo aplica para el capacitivo del nodo 1
                    QString replace_cap_1 = ui->line_lc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_1 = engine_functions_cap_1.evaluate(replace_cap_1);

                    watercut_for_sensor = master_value_cap_1.toNumber();
                }
            }else{
                watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;
            }



            if(raw_norm_cap<=capa_water_f){
                //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
                raw_norm_cap=100;
            }
            else{
                if(raw_norm_cap<=capa_water){
                    // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
                }
                else{
                    if(raw_norm_cap<=7501){
                        watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=13801){
                            watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                        }
                        else{
                            if(raw_norm_cap<=15001){
                                watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                            }
                            else{
                                watercut_for_sensor=0;
                            }
                        }
                    }
                }
            }
            if(watercut_for_sensor > 100){
                watercut_for_sensor = 100;
            }
            list_wc_cap.append(watercut_for_sensor);
            //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
        }
        vector_watercut_capacitance.append(list_wc_cap);
    }
}

//Calcula el corte resistivo para el modelo turbulento
void jptwatercut::watercut_resistivity_turb(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    vector_watercut_resistivity_ei.clear();
    vector_watercut_resistivity_turb.clear();

    QList<float> list_wc_res;
    if(on_off_inverse==1){
        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                int total_counts_res = int(vector_res_inverse_emulsion[j][i] + vector_res_temp[j][i] + vector_res_salinity[j][i]);

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }

                list_wc_res.append(watercut_resis);
            }
            vector_watercut_resistivity_ei.append(list_wc_res);
        }
    }else{
        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                total_counts_res = int(_node_norm[j][i].get_res_norm());

                if(ui->check_th_gas->isChecked()){
                    if(total_counts_res > ui->spin_th_gas->value()){
                        int result = total_counts_res - (total_counts_res * 0.03);
                        total_counts_res = result;
                        qDebug()<<total_counts_res;
                    }
                }

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }
                list_wc_res.append(watercut_resis);
                //qDebug()<<"total res lam "<<list_wc_res[i]<<" i "<<i;

            }
            vector_watercut_resistivity_turb.append(list_wc_res);
        }
    }
}

//Calcula el corte capacitivo para el modelo turbulento
void jptwatercut::watercut_capacitive_turb(){
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    QList<float> list_wc_cap;
    vector_watercut_capacitance_turb.clear();

    //con estos valores debo de generar mi curva dinamica.
    /*
     * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
     *
     */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;

    for (int j=0;j<min;j++) {
        list_wc_cap.clear();
        for (int i=0;i<6;i++) {
            int raw_norm_cap=0;

            raw_norm_cap = _node_norm[j][i].get_cap_norm();

            if(ui->check_th_gas->isChecked()){
                if(raw_norm_cap > ui->spin_th_gas->value()){
                    int result = raw_norm_cap - (raw_norm_cap * 0.03);
                    raw_norm_cap = result;
                    qDebug()<<raw_norm_cap;
                }
            }

            float watercut_for_sensor=0;

            if(ui->radio_hc_hf->isChecked()){
                //alto corte, alto flujo
                watercut_for_sensor = (-0.0002 * raw_norm_cap) + 2.8;
            }else if(ui->radio_lc_hf->isChecked()){
                //bajo corte, alto flujo
                watercut_for_sensor = (-0.000008 * raw_norm_cap) + 1.2385;
            }else if(ui->radio_hc_lf->isChecked()){
                //alto corte, bajo flujo
                if(i == 3){
                    QScriptEngine engine_functions_cap_4;
                    QScriptValue master_value_cap_4;

                    //solo aplica para el capacitivo del nodo 4
                    QString replace_cap_4 = ui->line_hc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_4 = engine_functions_cap_4.evaluate(replace_cap_4);

                    watercut_for_sensor = master_value_cap_4.toNumber();
                }

            }else if(ui->radio_lc_lf->isChecked()){
                //bajo corte, bajo flujo
                if(i == 0){
                    QScriptEngine engine_functions_cap_1;
                    QScriptValue master_value_cap_1;

                    //solo aplica para el capacitivo del nodo 1
                    QString replace_cap_1 = ui->line_lc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_1 = engine_functions_cap_1.evaluate(replace_cap_1);

                    watercut_for_sensor = master_value_cap_1.toNumber();
                }
            }else{
                watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;
            }



            if(raw_norm_cap<=capa_water_f){
                //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
                raw_norm_cap=100;
            }
            else{
                if(raw_norm_cap<=capa_water){
                    // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
                }
                else{
                    if(raw_norm_cap<=7501){
                        watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=13801){
                            watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                        }
                        else{
                            if(raw_norm_cap<=15001){
                                watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                            }
                            else{
                                watercut_for_sensor=0;
                            }
                        }
                    }
                }
            }
            if(watercut_for_sensor > 100){
                watercut_for_sensor = 100;
            }
            list_wc_cap.append(watercut_for_sensor);
            //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
        }
        vector_watercut_capacitance_turb.append(list_wc_cap);
    }

    calcule_model_turbulent();
}

//Calcula el corte capacitivo para el modelo laminar emulsion inversa
void jptwatercut::watercut_capacitive_EI(){
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    QList<float> list_wc_cap_ei;
    vector_cap_inverse_emulsion_final.clear();

    //con estos valores debo de generar mi curva dinamica.
    /*
     * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
     *
     */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;
    int e = min - 1;
    for (int j=0;j<min;j++) {
        list_wc_cap_ei.clear();
        for (int i=0;i<6;i++) {
            int raw_norm_cap=0;
            if(vector_cap_inverse_emulsion[j][i] == 0){
                raw_norm_cap = _node_norm[j][i].get_cap_norm();
            }else{
                raw_norm_cap = int(vector_cap_inverse_emulsion[j][i]);
            }

            float watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;
            //qDebug()<<"8888- "<<watercut_for_sensor;
            if(raw_norm_cap<=capa_water_f){
                //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
                raw_norm_cap=100;
            }
            else{
                if(raw_norm_cap<=capa_water){
                    // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
                }
                else{
                    if(raw_norm_cap<=7501){
                        watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=13801){
                            watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                        }
                        else{
                            if(raw_norm_cap<=15001){
                                watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                            }
                            else{
                                watercut_for_sensor=0;
                            }
                        }
                    }
                }
            }
            if(watercut_for_sensor > 100){
                watercut_for_sensor = 100;
            }
            list_wc_cap_ei.append(watercut_for_sensor);
            //qDebug()<<"wc capacitivo "<<watercut_for_sensor<<" i "<<i;
        }
        vector_cap_inverse_emulsion_final.append(list_wc_cap_ei);
    }

    calcule_model_laminar_EI();
}

//Calcula el corte resistivo turbulento laminar
void jptwatercut::watercut_resistivity_turb_lam(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    vector_watercut_resistivity_ei.clear();
    vector_watercut_resistivity_lam_turb.clear();

    QList<float> list_wc_res;
    if(on_off_inverse==1){
        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                int total_counts_res = int(vector_res_inverse_emulsion[j][i] + vector_res_temp[j][i] + vector_res_salinity[j][i]);

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }

                list_wc_res.append(watercut_resis);
            }
            vector_watercut_resistivity_ei.append(list_wc_res);
        }
    }else{
        //Pendiente
        float m = (-100)/float((resis_oil-resis_water));
        //Vector Y0
        float Y0 = float((-m*resis_water)+100);

        for (int j=0;j<min;j++) {
            list_wc_res.clear();
            for (int i=0;i<6;i++) {
                total_counts_res = int(_node_norm[j][i].get_res_norm());

                float watercut_resis = float((m*total_counts_res)+Y0);
                if(watercut_resis>=100){
                    watercut_resis = 100;
                }else if(watercut_resis<=0){
                    watercut_resis = 0;
                }
                list_wc_res.append(watercut_resis);
                //qDebug()<<"total res lam "<<list_wc_res[i]<<" i "<<i;

            }
            vector_watercut_resistivity_lam_turb.append(list_wc_res);
        }
    }
}

//Calcula el corte capacitivo turbulento laminar
void jptwatercut::watercut_capacitive_turb_lam(){
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    QList<float> list_wc_cap;
    vector_watercut_capacitance_lam_turb.clear();

    //con estos valores debo de generar mi curva dinamica.
    /*
     * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
     *
     */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;

    for (int j=0;j<min;j++) {
        list_wc_cap.clear();
        for (int i=0;i<6;i++) {
            int raw_norm_cap=0;

            raw_norm_cap = _node_norm[j][i].get_cap_norm();

            float watercut_for_sensor=0;

            if(ui->radio_hc_hf->isChecked()){
                //alto corte, alto flujo
                watercut_for_sensor = (-0.0002 * raw_norm_cap) + 2.8;
            }else if(ui->radio_lc_hf->isChecked()){
                //bajo corte, alto flujo
                watercut_for_sensor = (-0.000008 * raw_norm_cap) + 1.2385;
            }else if(ui->radio_hc_lf->isChecked()){
                //alto corte, bajo flujo
                if(i == 3){
                    QScriptEngine engine_functions_cap_4;
                    QScriptValue master_value_cap_4;

                    //solo aplica para el capacitivo del nodo 4
                    QString replace_cap_4 = ui->line_hc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_4 = engine_functions_cap_4.evaluate(replace_cap_4);

                    watercut_for_sensor = master_value_cap_4.toNumber();
                }

            }else if(ui->radio_lc_lf->isChecked()){
                //bajo corte, bajo flujo
                if(i == 0){
                    QScriptEngine engine_functions_cap_1;
                    QScriptValue master_value_cap_1;

                    //solo aplica para el capacitivo del nodo 1
                    QString replace_cap_1 = ui->line_lc_lf->text().replace("X", QString::number(raw_norm_cap));
                    master_value_cap_1 = engine_functions_cap_1.evaluate(replace_cap_1);

                    watercut_for_sensor = master_value_cap_1.toNumber();
                }
            }else{
                watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;
            }



            if(raw_norm_cap<=capa_water_f){
                //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
                raw_norm_cap=100;
            }
            else{
                if(raw_norm_cap<=capa_water){
                    // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
                }
                else{
                    if(raw_norm_cap<=7501){
                        watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=13801){
                            watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                        }
                        else{
                            if(raw_norm_cap<=15001){
                                watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                            }
                            else{
                                watercut_for_sensor=0;
                            }
                        }
                    }
                }
            }
            list_wc_cap.append(watercut_for_sensor);
            //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
        }
        vector_watercut_capacitance_lam_turb.append(list_wc_cap);
    }

    calcule_model_turbulent_lam();
}

/*
 * El modelo laminar lo genera a partir de ciertas validaciones:
 *
 * Este modelo va de acuerdo a los nodos 1 y 4
 *
 * El valor resistivo 1 debe ser menor a el valor del threshold que esta en la interfaz (WATER)
 * El valor capacitivo 1 debe ser mayor a el valor del threshold que esta en la interfaz (OIL)
 * El valor resistivo 4 debe ser menor a el valor del threshold que esta en la interfaz (WATER)
 * El valor capacitivo 4 debe ser mayor a el valor del threshold que esta en la interfaz (OIL)
 *
 * Si todos coinciden, entonces genera los cortes capacitivos y resistivos. Valor del regimen y Plot del mismo.
*/
void jptwatercut::regimen_laminar(){
    int resis1 =0, resis2 =0, resis3 =0, resis4 =0, resis5 =0, resis6 =0,
            capa1=0, capa2 =0, capa3 =0, capa4 =0, capa5 =0, capa6=0,
            temp=0, sal=0, wc_res=0, wc_cap=0;

    int avg_resis1 =0, avg_resis2 =0, avg_resis3 =0, avg_resis4 =0, avg_resis5 =0,
            avg_resis6 =0, avg_capa1 =0,avg_capa2 =0, avg_capa3 =0, avg_capa4 =0,
            avg_capa5 =0, avg_capa6=0, avg_temp=0, avg_salinidad=0, avg_wc_res=0, avg_wc_cap=0;

    int counter_true=0;

    for (int j=0;j<min;j++) {
        int temp_res1=0, temp_res4=0, temp_cap1=0, temp_cap4=0;
        //Recolectamos los diferentes valores acumulativos de todos los sensores para calcular
        resis1 += _node_norm[j][0].get_res_norm();
        capa1 += _node_norm[j][0].get_cap_norm();

        resis2 += _node_norm[j][1].get_res_norm();
        capa2 += _node_norm[j][1].get_cap_norm();

        resis3 += _node_norm[j][2].get_res_norm();
        capa3 += _node_norm[j][2].get_cap_norm();

        resis4 += _node_norm[j][3].get_res_norm();
        capa4 += _node_norm[j][3].get_cap_norm();

        resis5 += _node_norm[j][4].get_res_norm();
        capa5 += _node_norm[j][4].get_cap_norm();

        resis6 += _node_norm[j][5].get_res_norm();
        capa6 += _node_norm[j][5].get_cap_norm();

        temp += int(database->get_data_all_nodes_old()[6][j].get_eng_tempe());

        sal += vector_real_salinity[j];
        /*
        for (int i=0;i<6;i++) {
            wc_res+=int(vector_watercut_resistivity[j][i]);
            wc_cap+=int(vector_watercut_capacitance[j][i]);
        }

        //se agrega las temporales para calculo de true y false
        temp_res1 = vector_res_norma[j][0] + vector_res_temp[j][0] + vector_res_salinity[j][0];
        temp_cap1 = vector_capa_norma[j][0] + vector_capa_temp[j][0] + vector_capa_salinity[j][0];

        temp_res4 = vector_res_norma[j][3] + vector_res_temp[j][3] + vector_res_salinity[j][3];
        temp_cap4 = vector_capa_norma[j][3] + vector_capa_temp[j][3] + vector_capa_salinity[j][3];

        if(temp_res1<ui->spin_value_th_water_lam->value() && temp_res4>ui->spin_value_th_oil_lam->value() &&
                temp_cap1<ui->spin_value_th_water_lam->value() && temp_cap4>ui->spin_value_th_oil_lam->value()){
            //qDebug()<<"TRUE LAMINAR";
            counter_true++;
        }else{
            //qDebug()<<"FALSE LAMINAR";
        }
        */
    }

    //calculo para porcentaje de cumplimiento de modelo


    avg_resis1 = resis1/min;
    avg_resis2 = resis2/min;
    avg_resis3 = resis3/min;
    avg_resis4 = resis4/min;
    avg_resis5 = resis5/min;
    avg_resis6 = resis6/min;
    //
    avg_capa1 = capa1/min;
    avg_capa2 = capa2/min;
    avg_capa3 = capa3/min;
    avg_capa4 = capa4/min;
    avg_capa5 = capa5/min;
    avg_capa6 = capa6/min;

    avg_temp = temp/min;
    avg_salinidad = sal/min;

    avg_wc_res = wc_res/min;
    avg_wc_cap = wc_cap/min;

    //se agrega la parte de validacion desde el threshold para agua y oil
    if(avg_resis1<ui->spin_value_th_water_lam_resis->value() && avg_resis4>ui->spin_value_th_oil_lam_resis->value() &&
            avg_capa1<ui->spin_value_th_water_lam_capa->value() && avg_capa4>ui->spin_value_th_oil_lam_capa->value()){
        qDebug()<<"LAMINAR";
        watercut_resistivity();
        watercut_capacitive();
        calcule_model_laminar();
        plot_wc_laminar();
        //calcule_model_laminar_EI();
    }else{
        qDebug()<<"No cumple laminar";
    }

    //Para calcular turbulento
    //con los valores de media procedemos a evaluar cada valor en un lapso de intervalo del 10% o del 30% vaiale en el sistema.
    //para llos calculamo los valoes a evaluar.
    //obtengo primero el valor del procentaje de variacion.

}

//Hace una resta, y genera con valor a absoluto
int jptwatercut::subs_values(int value1, int value2){
    int subs = 0;
    subs  = abs(value1 - value2);
    return subs;
}

//Hace los saltos de las fechas en los diferentes plots
bool jptwatercut::event_plot(int min, int counter){
    if(min<250){
        if(counter==10){
            return true;
        }
    }else if(min<500){
        if(counter==20){
            return true;
        }
    }else if(min<750){
        if(counter==30){
            return true;
        }
    }else if(min<1000){
        if(counter==40){
            return true;
        }
    }else if(min<1250){
        if(counter==50){
            return true;
        }
    }else if(min<1500){
        if(counter==60){
            return true;
        }
    }else if(min<1750){
        if(counter==70){
            return true;
        }
    }else if(min<2000){
        if(counter==80){
            return true;
        }
    }else if(min<2250){
        if(counter==90){
            return true;
        }
    }else if(min<2500){
        if(counter==100){
            return true;
        }
    }else if(min<2750){
        if(counter==110){
            return true;
        }
    }else if(min<3000){
        if(counter==120){
            return true;
        }
    }else if(min<3250){
        if(counter==130){
            return true;
        }
    }else if(min<3500){
        if(counter==140){
            return true;
        }
    }else if(min<3750){
        if(counter==150){
            return true;
        }
    }else if(min<4000){
        if(counter==160){
            return true;
        }
    }
    return false;
}

/*
 * El modelo turbulento se valida a partir de todos los sensores
 * Cada nodo se resta en conjunto de los otros, esto quiere decir:
 *
 * Que para el nodo1, se resta con los demas, pero no con el mismo
 * y asi pasa con el resto de los sensores.
 *
 * Esta funcion aun esta en fase experimental.
 *
 * Aun asi se ejecuta el corte capacitivo y resitivo; plotea el valor del corte laminar
*/
void jptwatercut::regimen_turbulent(){

    int resis1 =0, resis2 =0, resis3 =0, resis4 =0, resis5 =0, resis6 =0,
            capa1=0, capa2 =0, capa3 =0, capa4 =0, capa5 =0, capa6=0,
            temp=0, sal=0, wc_res=0, wc_cap=0;

    int avg_resis1 =0, avg_resis2 =0, avg_resis3 =0, avg_resis4 =0, avg_resis5 =0,
            avg_resis6 =0, avg_capa1 =0,avg_capa2 =0, avg_capa3 =0, avg_capa4 =0,
            avg_capa5 =0, avg_capa6=0, avg_temp=0, avg_salinidad=0, avg_wc_res=0, avg_wc_cap=0;

    QList<int> list_res, list_cap;
    QList<bool> list_bool_res, list_bool_cap;

    list_cap.clear();
    list_res.clear();
    list_bool_cap.clear();
    list_bool_res.clear();

    int spin_flow = int(ui->spin_value_cps_tur->value());

    float counter_true = 0;

    for (int j=0;j<min;j++) {
        int temp_resis1 =0, temp_resis2 =0, temp_resis3 =0, temp_resis4 =0, temp_resis5 =0,
                temp_resis6 =0, temp_capa1=0, temp_capa2 =0, temp_capa3 =0, temp_capa4 =0,
                temp_capa5 =0, temp_capa6=0;
        list_cap.clear();
        list_res.clear();

        //Recolectamos los diferentes valores acumulativos de todos los sensores para calcular
        resis1 +=_node_norm[j][0].get_res_norm();
        capa1 += _node_norm[j][0].get_cap_norm();

        resis2 += _node_norm[j][1].get_res_norm();
        capa2 += _node_norm[j][1].get_cap_norm();

        resis3 += _node_norm[j][2].get_res_norm();
        capa3 += _node_norm[j][2].get_cap_norm();

        resis4 += _node_norm[j][3].get_res_norm();
        capa4 += _node_norm[j][3].get_cap_norm();

        resis5 += _node_norm[j][4].get_res_norm();
        capa5 += _node_norm[j][4].get_cap_norm();

        resis6 += _node_norm[j][5].get_res_norm();
        capa6 += _node_norm[j][5].get_cap_norm();

        temp += int(database->get_data_all_nodes_old()[6][j].get_eng_tempe());

        sal += vector_real_salinity[j];
        /*
        for (int i=0;i<6;i++) {
            wc_res+=int(vector_watercut_resistivity[j][i]);
            wc_cap+=int(vector_watercut_capacitance[j][i]);
        }

        temp_resis1 = vector_res_norma[j][0] + vector_res_temp[j][0] + vector_res_salinity[j][0];
        temp_capa1 = vector_capa_norma[j][0] + vector_capa_temp[j][0] + vector_capa_salinity[j][0];

        temp_resis2 = vector_res_norma[j][1] + vector_res_temp[j][1] + vector_res_salinity[j][1];
        temp_capa2 = vector_capa_norma[j][1] + vector_capa_temp[j][1] + vector_capa_salinity[j][1];

        temp_resis3 = vector_res_norma[j][2] + vector_res_temp[j][2] + vector_res_salinity[j][2];
        temp_capa3 = vector_capa_norma[j][2] + vector_capa_temp[j][2] + vector_capa_salinity[j][2];

        temp_resis4 = vector_res_norma[j][3] + vector_res_temp[j][3] + vector_res_salinity[j][3];
        temp_capa4 = vector_capa_norma[j][3] + vector_capa_temp[j][3] + vector_capa_salinity[j][3];

        temp_resis5 = vector_res_norma[j][4] + vector_res_temp[j][4] + vector_res_salinity[j][4];
        temp_capa5 = vector_capa_norma[j][4] + vector_capa_temp[j][4] + vector_capa_salinity[j][4];

        temp_resis6 = vector_res_norma[j][5] + vector_res_temp[j][5] + vector_res_salinity[j][5];
        temp_capa6 = vector_capa_norma[j][5] + vector_capa_temp[j][5] + vector_capa_salinity[j][5];

        //se llenan las listas resistivas
        list_res.append(temp_resis1);
        list_res.append(temp_resis2);
        list_res.append(temp_resis3);
        list_res.append(temp_resis4);
        list_res.append(temp_resis5);
        list_res.append(temp_resis6);
        //se llenan las listas capacitivas
        list_cap.append(temp_capa1);
        list_cap.append(temp_capa2);
        list_cap.append(temp_capa3);
        list_cap.append(temp_capa4);
        list_cap.append(temp_capa5);
        list_cap.append(temp_capa6);

        //se hace el contador con 1/60 ya que son 30 evaluaciones de resistivos y 30 capacitivos

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_res[i], list_res[j]);
                    if(value <= spin_flow){
                        counter_true+=0.01666;
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_cap[i], list_cap[j]);
                    if(value <= spin_flow){
                        counter_true+=0.01666;
                    }
                }
            }
        }
        */

    }

    //resistividad
    avg_resis1 = resis1/min;
    avg_resis2 = resis2/min;
    avg_resis3 = resis3/min;
    avg_resis4 = resis4/min;
    avg_resis5 = resis5/min;
    avg_resis6 = resis6/min;
    //Llenamos las listas de los sensores resistividad
    list_res.append(avg_resis1);
    list_res.append(avg_resis2);
    list_res.append(avg_resis3);
    list_res.append(avg_resis4);
    list_res.append(avg_resis5);
    list_res.append(avg_resis6);

    //capacitance
    avg_capa1 = capa1/min;
    avg_capa2 = capa2/min;
    avg_capa3 = capa3/min;
    avg_capa4 = capa4/min;
    avg_capa5 = capa5/min;
    avg_capa6 = capa6/min;
    //Llenamos las listas de los sensores capacitancia
    list_cap.append(avg_capa1);
    list_cap.append(avg_capa2);
    list_cap.append(avg_capa3);
    list_cap.append(avg_capa4);
    list_cap.append(avg_capa5);
    list_cap.append(avg_capa6);

    avg_temp = temp/min;
    avg_salinidad = sal/min;

    avg_wc_res = wc_res/min;
    avg_wc_cap = wc_cap/min;

    //Para calcular turbulento
    //La logica consta de que se debe evaluar todos los sensores resistivos entre si, sacando la diferencia esperada del spin
    //lo mismo se hace los sensores capacitivos y se debe de sacar con valor absoluto

    //este es primero para evaluar los valores resistivos
    for (int i=0;i<6;i++) {
        qDebug()<<"value lista res "<<list_res[i];
        for (int j = 0; j < 6; ++j) {
            if(i!=j){
                int value=0;
                value = subs_values(list_res[i], list_res[j]);
                if(value <= spin_flow){
                    list_bool_res.append(true);
                    qDebug()<<"value "<<value<<i+1<<" "<<j+1;
                }else{
                    list_bool_res.append(false);
                }
            }
        }
    }

    for (int i=0;i<6;i++) {
        qDebug()<<"value lista cap "<<list_cap[i];
        for (int j = 0; j < 6; ++j) {
            if(i!=j){
                int value=0;
                value = subs_values(list_cap[i], list_cap[j]);
                if(value <= spin_flow){
                    list_bool_cap.append(true);
                    qDebug()<<"value "<<value <<i+1<<" "<<j+1;
                }else{
                    list_bool_cap.append(false);
                }
            }
        }
    }
    /*
    for (int h=0;h<list_bool_cap.size();h++) {
        if(list_bool_res[h] && list_bool_cap[h]){
            counter_true++;
        }
    }*/
    /*
    if(counter_cap == min && counter_res == min){
    }
    */

    watercut_resistivity_turb();
    watercut_capacitive_turb();
    plot_wc_turb();
}

/*
 * El modelo turbulento laminar se valida a partir de los nodos 2,4,5,6
 * Cada nodo se resta en conjunto de los otros, esto quiere decir:
 *
 * Esto quiere decir que primero debe cumplir de si son laminar
 * Esto valida tal caul como en el regimen laminar
 *
 * Que para el nodo2, se resta con los demas, pero no con el mismo
 * y asi pasa con el resto de los sensores.
 *
 * Pero en este caso no se usa ni el nodo1 y el 4 por que ya se usaron para
 * validar laminar
 *
 * Esta funcion aun esta en fase experimental.
 *
 * Aun asi se ejecuta el corte capacitivo y resitivo; plotea el valor del corte laminar
*/
void jptwatercut::regimen_turbulent_laminar(){

    int resis1 =0, resis2 =0, resis3 =0, resis4 =0, resis5 =0, resis6 =0,
            capa1=0, capa2 =0, capa3 =0, capa4 =0, capa5 =0, capa6=0,
            temp=0, sal=0, wc_res=0, wc_cap=0;

    int avg_resis1 =0, avg_resis2 =0, avg_resis3 =0, avg_resis4 =0, avg_resis5 =0,
            avg_resis6 =0, avg_capa1 =0,avg_capa2 =0, avg_capa3 =0, avg_capa4 =0,
            avg_capa5 =0, avg_capa6=0, avg_temp=0, avg_salinidad=0, avg_wc_res=0, avg_wc_cap=0;

    QList<int> list_res, list_cap;
    QList<bool> list_bool_res, list_bool_cap;

    list_cap.clear();
    list_res.clear();
    list_bool_cap.clear();
    list_bool_res.clear();

    float counter_true_lam=0, counter_true_tur=0;

    int spin_flow = int(ui->spin_value_cps_tur->value());
    for (int j=0;j<min;j++) {
        int temp_resis1 =0, temp_resis2 =0, temp_resis3 =0, temp_resis4 =0, temp_resis5 =0,
                temp_resis6 =0, temp_capa1=0, temp_capa2 =0, temp_capa3 =0, temp_capa4 =0,
                temp_capa5 =0, temp_capa6=0;
        list_cap.clear();
        list_res.clear();

        //Recolectamos los diferentes valores acumulativos de todos los sensores para calcular
        resis1 += _node_norm[j][0].get_res_norm();
        capa1 += _node_norm[j][0].get_cap_norm();

        resis2 += _node_norm[j][1].get_res_norm();
        capa2 += _node_norm[j][1].get_cap_norm();

        resis3 += _node_norm[j][2].get_res_norm();
        capa3 += _node_norm[j][2].get_cap_norm();

        resis4 += _node_norm[j][3].get_res_norm();
        capa4 += _node_norm[j][3].get_cap_norm();

        resis5 += _node_norm[j][4].get_res_norm();
        capa5 += _node_norm[j][4].get_cap_norm();

        resis6 += _node_norm[j][5].get_res_norm();
        capa6 += _node_norm[j][5].get_cap_norm();

        temp += int(database->get_data_all_nodes_old()[6][j].get_eng_tempe());

        sal += vector_real_salinity[j];
        /*
        for (int i=0;i<6;i++) {
            wc_res+=int(vector_watercut_resistivity[j][i]);
            wc_cap+=int(vector_watercut_capacitance[j][i]);
        }

        if(resis1<ui->spin_value_th_water_tur->value() && resis4>ui->spin_value_th_oil_tur->value() &&
                capa1<ui->spin_value_th_water_tur->value() && capa4>ui->spin_value_th_oil_tur->value()){
            counter_true_lam++;
        }

        temp_resis1 = vector_res_norma[j][0] + vector_res_temp[j][0] + vector_res_salinity[j][0];
        temp_capa1 = vector_capa_norma[j][0] + vector_capa_temp[j][0] + vector_capa_salinity[j][0];

        temp_resis2 = vector_res_norma[j][1] + vector_res_temp[j][1] + vector_res_salinity[j][1];
        temp_capa2 = vector_capa_norma[j][1] + vector_capa_temp[j][1] + vector_capa_salinity[j][1];

        temp_resis3 = vector_res_norma[j][2] + vector_res_temp[j][2] + vector_res_salinity[j][2];
        temp_capa3 = vector_capa_norma[j][2] + vector_capa_temp[j][2] + vector_capa_salinity[j][2];

        temp_resis4 = vector_res_norma[j][3] + vector_res_temp[j][3] + vector_res_salinity[j][3];
        temp_capa4 = vector_capa_norma[j][3] + vector_capa_temp[j][3] + vector_capa_salinity[j][3];

        temp_resis5 = vector_res_norma[j][4] + vector_res_temp[j][4] + vector_res_salinity[j][4];
        temp_capa5 = vector_capa_norma[j][4] + vector_capa_temp[j][4] + vector_capa_salinity[j][4];

        temp_resis6 = vector_res_norma[j][5] + vector_res_temp[j][5] + vector_res_salinity[j][5];
        temp_capa6 = vector_capa_norma[j][5] + vector_capa_temp[j][5] + vector_capa_salinity[j][5];

        //se llenan las listas resistivas
        list_res.append(temp_resis1);
        list_res.append(temp_resis2);
        list_res.append(temp_resis3);
        list_res.append(temp_resis4);
        list_res.append(temp_resis5);
        list_res.append(temp_resis6);
        //se llenan las listas capacitivas
        list_cap.append(temp_capa1);
        list_cap.append(temp_capa2);
        list_cap.append(temp_capa3);
        list_cap.append(temp_capa4);
        list_cap.append(temp_capa5);
        list_cap.append(temp_capa6);

        //se hace el contador con 1/40 ya que son 20 evaluaciones de resistivos y 20 capacitivos

        for (int i=0;i<6;i++) {
            //qDebug()<<"value lista res "<<list_res[i];
            for (int j=0;j<6;++j) {
                if(i!=0 && i!=3 && i!=j){
                    if(j!=0 && j!=3){
                        int value=0;
                        value = subs_values(list_res[i], list_res[j]);
                        if(value <= spin_flow){
                            counter_true_tur+=0.025;
                        }
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            //qDebug()<<"value lista cap "<<list_cap[i];
            for (int j=0;j<6;++j) {
                if(i!=0 && i!=3 && i!=j){
                    if(j!=0 && j!=3){
                        int value=0;
                        value = subs_values(list_cap[i], list_cap[j]);
                        if(value <= spin_flow){
                            counter_true_tur+=0.025;
                        }
                    }
                }
            }
        }
        */
    }

    //resistividad
    avg_resis1 = resis1/min;
    avg_resis2 = resis2/min;
    avg_resis3 = resis3/min;
    avg_resis4 = resis4/min;
    avg_resis5 = resis5/min;
    avg_resis6 = resis6/min;
    //Llenamos las listas de los sensores resistividad
    list_res.append(avg_resis1);
    list_res.append(avg_resis2);
    list_res.append(avg_resis3);
    list_res.append(avg_resis4);
    list_res.append(avg_resis5);
    list_res.append(avg_resis6);

    //capacitance
    avg_capa1 = capa1/min;
    avg_capa2 = capa2/min;
    avg_capa3 = capa3/min;
    avg_capa4 = capa4/min;
    avg_capa5 = capa5/min;
    avg_capa6 = capa6/min;
    //Llenamos las listas de los sensores capacitancia
    list_cap.append(avg_capa1);
    list_cap.append(avg_capa2);
    list_cap.append(avg_capa3);
    list_cap.append(avg_capa4);
    list_cap.append(avg_capa5);
    list_cap.append(avg_capa6);

    avg_temp = temp/min;
    avg_salinidad = sal/min;

    avg_wc_res = wc_res/min;
    avg_wc_cap = wc_cap/min;

    //Para calcular turbulento
    //La logica consta de que se debe evaluar todos los sensores resistivos entre si, sacando la diferencia esperada del spin
    //lo mismo se hace los sensores capacitivos y se debe de sacar con valor absoluto

    //se queda aqui para terminar de configurar el modelo de turbulento laminar

    //este es primero para evaluar los valores resistivos


    if(avg_resis1<ui->spin_value_th_water_tur_res->value() && avg_resis4>ui->spin_value_th_oil_tur_res->value() &&
            avg_capa1<ui->spin_value_th_water_tur_cap->value() && avg_capa4>ui->spin_value_th_oil_tur_cap->value()){
        for (int i=0;i<6;i++) {
            //qDebug()<<"value lista res "<<list_res[i];
            for (int j=0;j<6;++j) {
                if(i!=0 && i!=3 && i!=j){
                    if(j!=0 && j!=3){
                        int value=0;
                        value = subs_values(list_res[i], list_res[j]);
                        if(value <= spin_flow){
                            list_bool_res.append(true);
                            qDebug()<<"value "<<value<<i+1<<" "<<j+1;
                        }else{
                            list_bool_res.append(false);
                        }
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            //qDebug()<<"value lista cap "<<list_cap[i];
            for (int j=0;j<6;++j) {
                if(i!=0 && i!=3 && i!=j){
                    if(j!=0 && j!=3){
                        int value=0;
                        value = subs_values(list_cap[i], list_cap[j]);
                        if(value <= spin_flow){
                            list_bool_cap.append(true);
                            qDebug()<<"value "<<value <<i+1<<" "<<j+1;
                        }else{
                            list_bool_cap.append(false);
                        }
                    }
                }
            }
        }
    }else{
        qDebug()<<"No cumple capa";
    }


    int counter_res=0;
    int counter_cap=0;
    /*
    for (int h=0;h<list_bool_cap.size();h++) {
        if(list_bool_res[h]){
            counter_res++;
        }
        if(list_bool_cap[h]){
            counter_cap++;
        }
        if(!list_bool_res[h] || !list_bool_cap[h]){
            break;
        }
    }

    if(counter_cap == min && counter_res == min){
        watercut_capacitive_turb();
        watercut_resistivity_turb();
    }
    */
    watercut_resistivity_turb_lam();
    watercut_capacitive_turb_lam();
    plot_wc_turb_lam();
    calcule_model_turbulent_lam();

}

/*
 * El modelo laminar emulsion inversa se calcula solamente desde los sensores capacitivos
 * y se ajusta por medio de un factor
*/
void jptwatercut::data_inverse_model(){
    //AGREGAR VECTORES A MODELO INVERSO
    float avg_resis_1=0, avg_resis_2=0, avg_resis_3=0, avg_resis_4=0, avg_resis_5=0, avg_resis_6=0,
            avg_cap_1=0, avg_cap_2=0, avg_cap_3=0, avg_cap_4=0, avg_cap_5=0, avg_cap_6=0;

    float factor =0;

    QList<float> list_res, list_cap;
    list_res.clear();
    list_cap.clear();
    QList<QList<bool>> val_resis, val_cap;
    QList<bool> list_val_resis, list_val_cap;

    vector_res_inverse_emulsion.clear();
    vector_cap_inverse_emulsion.clear();

    //se hace el ajuste de API
    factor = (-0.05 * float(ui->spin_factor->value())) + 1.2;

    int condi =ui->spin_value_th_oil_lei->value(), diff =ui->spin_value_th_diff_lei->value();

    float counter_true=0;
    /*
    for (int g=0;g<min;g++) {
        for (int i=0;i<6;i++) {
            list_val_resis.append(false);
            list_val_cap.append(false);
        }
        val_resis.append(list_val_resis);
        val_cap.append(list_val_cap);
    }
    */
    //Average resistivo
    for (int j=0;j<min;j++) {
        float sum_true=0;

        //list_res.clear();
        //list_cap.clear();
        list_val_resis.clear();
        list_val_cap.clear();

        avg_resis_1=_node_norm[j][0].get_res_norm();
        avg_cap_1 =_node_norm[j][0].get_cap_norm();

        avg_resis_2=_node_norm[j][1].get_res_norm();
        avg_cap_2 =_node_norm[j][1].get_cap_norm();

        avg_resis_3=_node_norm[j][2].get_res_norm();
        avg_cap_3 =_node_norm[j][2].get_cap_norm();

        avg_resis_4=_node_norm[j][3].get_res_norm();
        avg_cap_4 =_node_norm[j][3].get_cap_norm();

        avg_resis_5=_node_norm[j][4].get_res_norm();
        avg_cap_5 =_node_norm[j][4].get_cap_norm();

        avg_resis_6=_node_norm[j][5].get_res_norm();
        avg_cap_6 =_node_norm[j][5].get_cap_norm();

        if(avg_resis_1>condi && on_off_inverse==1){
            if(avg_cap_1<avg_resis_1 && (avg_resis_1-avg_cap_1)>diff){

                list_val_resis.append(true);
                list_val_cap.append(true);

                //resis * factor
                list_res.append(_node_norm[j][0].get_res_norm() * ui->spin_factor->value());

                //cap * factor
                list_cap.append(_node_norm[j][0].get_cap_norm() * ui->spin_factor->value());

                //sum_true+=0.16667;

            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        if(avg_resis_2>condi && on_off_inverse==1){
            if(avg_cap_2<avg_resis_2 && (avg_resis_2-avg_cap_2)>diff){
                list_val_resis.append(true);
                list_val_cap.append(true);

                //resis * factor
                list_res.append(_node_norm[j][1].get_res_norm() * ui->spin_factor->value());

                //cap * factor
                list_cap.append(_node_norm[j][1].get_cap_norm() * ui->spin_factor->value());
                //sum_true+=0.16667;

            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        if(avg_resis_3>condi && on_off_inverse==1){
            if(avg_cap_3<avg_resis_3 && (avg_resis_3-avg_cap_3)>diff){
                list_val_resis.append(true);
                list_val_cap.append(true);


                //resis * factor
                list_res.append(_node_norm[j][2].get_res_norm() * ui->spin_factor->value());
                //cap * factor
                list_cap.append(_node_norm[j][2].get_cap_norm() * ui->spin_factor->value());
                //sum_true+=0.16667;
            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        if(avg_resis_4>condi && on_off_inverse==1){
            if(avg_cap_4<avg_resis_4 && (avg_resis_4-avg_cap_4)>diff){
                list_val_resis.append(true);
                list_val_cap.append(true);

                //resis * factor
                list_res.append(_node_norm[j][3].get_res_norm() * ui->spin_factor->value());
                //cap * factor
                list_cap.append(_node_norm[j][3].get_cap_norm() * ui->spin_factor->value());
                //sum_true+=0.16667;
            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        if(avg_resis_5>condi && on_off_inverse==1){
            if(avg_cap_5<avg_resis_5 && (avg_resis_5-avg_cap_5)>diff){
                list_val_resis.append(true);
                list_val_cap.append(true);

                //resis * factor
                list_res.append(_node_norm[j][4].get_res_norm() * ui->spin_factor->value());
                //cap * factor
                list_cap.append(_node_norm[j][4].get_cap_norm() * ui->spin_factor->value());
                //sum_true+=0.16667;
            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        if(avg_resis_6>condi && on_off_inverse==1){
            if(avg_cap_6<avg_resis_6 && (avg_resis_6-avg_cap_6)>diff){
                list_val_resis.append(true);
                list_val_cap.append(true);

                //resis * factor
                list_res.append(_node_norm[j][5].get_res_norm() * ui->spin_factor->value());
                //cap * factor
                list_cap.append(_node_norm[j][5].get_cap_norm() * ui->spin_factor->value());
                //sum_true+=0.16667;
            }else{
                //resis
                list_res.append(0);
                //cap
                list_cap.append(0);
                list_val_resis.append(false);
                list_val_cap.append(false);
            }
        }else{
            //resisr
            list_res.append(0);
            //cap
            list_cap.append(0);
            list_val_resis.append(false);
            list_val_cap.append(false);
        }

        val_resis.append(list_val_resis);
        val_cap.append(list_val_cap);
        vector_res_inverse_emulsion.append(list_res);
        vector_cap_inverse_emulsion.append(list_cap);
        qDebug()<<"suma "<<sum_true;

        counter_true+=sum_true;
        qDebug()<<"ct "<<counter_true;


    }

    for (int g=0;g<min;g++) {
        for (int i=0;i<6;i++) {
            if(val_resis[g][i] == true){
                // qDebug()<<"Sensor Resis "<<i+1<<" LEI ";
            }
            if(val_cap[g][i] == true){
                // qDebug()<<"Sensor Capa "<<i+1<<" LEI ";
            }
        }
    }


    watercut_capacitive_EI();
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_laminar(QMouseEvent *event){

    float x = ui->chart_wc_laminar->xAxis->pixelToCoord(event->pos().x());
    float y = ui->chart_wc_laminar->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("WC: " + QString::number(y,'f',2)));
}

//Permite la funcionalidad de zoom y movimiento en el plot de laminar
void jptwatercut::mouseWheel_laminar(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_wc_laminar->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar->xAxis,ui->chart_wc_laminar->yAxis);
        ui->chart_wc_laminar->axisRect()->setRangeZoom(ui->chart_wc_laminar->xAxis->orientation());
    }
    else if (ui->chart_wc_laminar->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar->xAxis,ui->chart_wc_laminar->yAxis);
        ui->chart_wc_laminar->axisRect()->setRangeZoom(ui->chart_wc_laminar->yAxis->orientation());
    }
    else if (ui->chart_wc_laminar->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar->xAxis2,ui->chart_wc_laminar->yAxis2);
        ui->chart_wc_laminar->axisRect()->setRangeZoom(ui->chart_wc_laminar->xAxis2->orientation());
    }
    else if (ui->chart_wc_laminar->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar->xAxis2,ui->chart_wc_laminar->yAxis2);
        ui->chart_wc_laminar->axisRect()->setRangeZoom(ui->chart_wc_laminar->yAxis2->orientation());
    }
    else
        ui->chart_wc_laminar->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_laminar_ei(QMouseEvent *event){

    float x = ui->chart_wc_laminar_ei->xAxis->pixelToCoord(event->pos().x());
    float y = ui->chart_wc_laminar_ei->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("WC: " + QString::number(y,'f',2)));
}

//Permite la funcionalidad de zoom y movimiento en el plot de laminar emulsion inversa
void jptwatercut::mouseWheel_laminar_ei(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_wc_laminar_ei->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar_ei->xAxis,ui->chart_wc_laminar_ei->yAxis);
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoom(ui->chart_wc_laminar_ei->xAxis->orientation());
    }
    else if (ui->chart_wc_laminar_ei->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar_ei->xAxis,ui->chart_wc_laminar_ei->yAxis);
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoom(ui->chart_wc_laminar_ei->yAxis->orientation());
    }
    else if (ui->chart_wc_laminar_ei->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar_ei->xAxis2,ui->chart_wc_laminar_ei->yAxis2);
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoom(ui->chart_wc_laminar_ei->xAxis2->orientation());
    }
    else if (ui->chart_wc_laminar_ei->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoomAxes(ui->chart_wc_laminar_ei->xAxis2,ui->chart_wc_laminar_ei->yAxis2);
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoom(ui->chart_wc_laminar_ei->yAxis2->orientation());
    }
    else
        ui->chart_wc_laminar_ei->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_turbulent(QMouseEvent *event){

    float x = ui->chart_wc_turbu->xAxis->pixelToCoord(event->pos().x());
    float y = ui->chart_wc_turbu->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("WC: " + QString::number(y,'f',2)));
}

//Permite la funcionalidad de zoom y movimiento en el plot de turbulento
void jptwatercut::mouseWheel_turbulent(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_wc_turbu->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turbu->axisRect()->setRangeZoomAxes(ui->chart_wc_turbu->xAxis,ui->chart_wc_turbu->yAxis);
        ui->chart_wc_turbu->axisRect()->setRangeZoom(ui->chart_wc_turbu->xAxis->orientation());
    }
    else if (ui->chart_wc_turbu->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turbu->axisRect()->setRangeZoomAxes(ui->chart_wc_turbu->xAxis,ui->chart_wc_turbu->yAxis);
        ui->chart_wc_turbu->axisRect()->setRangeZoom(ui->chart_wc_turbu->yAxis->orientation());
    }
    else if (ui->chart_wc_turbu->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turbu->axisRect()->setRangeZoomAxes(ui->chart_wc_turbu->xAxis2,ui->chart_wc_turbu->yAxis2);
        ui->chart_wc_turbu->axisRect()->setRangeZoom(ui->chart_wc_turbu->xAxis2->orientation());
    }
    else if (ui->chart_wc_turbu->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turbu->axisRect()->setRangeZoomAxes(ui->chart_wc_turbu->xAxis2,ui->chart_wc_turbu->yAxis2);
        ui->chart_wc_turbu->axisRect()->setRangeZoom(ui->chart_wc_turbu->yAxis2->orientation());
    }
    else
        ui->chart_wc_turbu->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera titulos en los plots para indicar valores en el punto donde se coloca el mouse
void jptwatercut::showPointToolTip_turbulent_lam(QMouseEvent *event){

    float x = ui->chart_wc_turb_lam->xAxis->pixelToCoord(event->pos().x());
    float y = ui->chart_wc_turb_lam->yAxis->pixelToCoord(event->pos().y());

    setToolTip(QString("WC: " + QString::number(y,'f',2)));
}

//Permite la funcionalidad de zoom y movimiento en el plot de turbulento laminar
void jptwatercut::mouseWheel_turbulent_lam(){
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->chart_wc_turb_lam->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turb_lam->axisRect()->setRangeZoomAxes(ui->chart_wc_turb_lam->xAxis,ui->chart_wc_turb_lam->yAxis);
        ui->chart_wc_turb_lam->axisRect()->setRangeZoom(ui->chart_wc_turb_lam->xAxis->orientation());
    }
    else if (ui->chart_wc_turb_lam->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turb_lam->axisRect()->setRangeZoomAxes(ui->chart_wc_turb_lam->xAxis,ui->chart_wc_turb_lam->yAxis);
        ui->chart_wc_turb_lam->axisRect()->setRangeZoom(ui->chart_wc_turb_lam->yAxis->orientation());
    }
    else if (ui->chart_wc_turb_lam->xAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turb_lam->axisRect()->setRangeZoomAxes(ui->chart_wc_turb_lam->xAxis2,ui->chart_wc_turb_lam->yAxis2);
        ui->chart_wc_turb_lam->axisRect()->setRangeZoom(ui->chart_wc_turb_lam->xAxis2->orientation());
    }
    else if (ui->chart_wc_turb_lam->yAxis2->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->chart_wc_turb_lam->axisRect()->setRangeZoomAxes(ui->chart_wc_turb_lam->xAxis2,ui->chart_wc_turb_lam->yAxis2);
        ui->chart_wc_turb_lam->axisRect()->setRangeZoom(ui->chart_wc_turb_lam->yAxis2->orientation());
    }
    else
        ui->chart_wc_turb_lam->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

//Genera el plot del corte laminar
void jptwatercut::plot_wc_laminar(){
    ui->chart_wc_laminar->clearGraphs();
    ui->chart_wc_laminar->clearItems();
    ui->chart_wc_laminar->clearPlottables();

    int flag=10;
    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_wc_laminar, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_laminar(QMouseEvent*)));

    connect(ui->chart_wc_laminar, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_laminar()));

    //tengo los dos vectores de corte resistivo y capacitivo
    //instancia que crea los tickers para las fechas
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    ui->chart_wc_laminar->addGraph();
    ui->chart_wc_laminar->addGraph();
    ui->chart_wc_laminar->addGraph();
    //WC total laminar
    ui->chart_wc_laminar->graph(0)->setPen(QPen(Qt::blue));
    ui->chart_wc_laminar->graph(0)->setName("WC Total Laminar %");
    //WC resis
    ui->chart_wc_laminar->graph(1)->setPen(QPen(Qt::red));
    ui->chart_wc_laminar->graph(1)->setName("WC Resistivity Laminar %");
    //WC capa
    ui->chart_wc_laminar->graph(2)->setPen(QPen(Qt::green));
    ui->chart_wc_laminar->graph(2)->setName("WC Capacitivity Laminar %");

    int counter=0;

    QVector<double> x(min), y(min), y1(min), y2(min);
    for (int i=0;i<min;i++) {
        double acum_res=0, acum_cap=0;
        for (int j=0;j<6;j++) {
            acum_res+=vector_watercut_resistivity[i][j] * area_sen[j];
            acum_cap+=vector_watercut_capacitance[i][j] * area_sen[j];
        }
        x[i]= i*5;
        //x[i]= i*100;
        y[i] = (acum_cap+acum_res)/2;

        y1[i] = acum_res;
        y2[i] = acum_cap;

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];

        if(i == 0){
            textTicker->addTick(x[i], date);
        }

        if(event_plot(min,counter)){
            textTicker->addTick(x[i], date);
            counter = 0;
        }
    }

    ui->chart_wc_laminar->xAxis->setTicker(textTicker);
    ui->chart_wc_laminar->graph(0)->setData(x, y);

    ui->chart_wc_laminar->graph(1)->setData(x, y1);

    ui->chart_wc_laminar->graph(2)->setData(x, y2);

    ui->chart_wc_laminar->graph(0)->rescaleAxes();
    //ui->chart_wc_laminar->graph(1)->rescaleAxes(true);

    ui->chart_wc_laminar->xAxis->setLabel("Date");
    ui->chart_wc_laminar->yAxis->setLabel("% Watercut Laminar");

    //label que muestra el chart
    ui->chart_wc_laminar->legend->setVisible(true);
    // set axes ranges, so we see all data:
    ui->chart_wc_laminar->xAxis->setRange(-1, min);
    ui->chart_wc_laminar->yAxis->setRange(0, min);
    ui->chart_wc_laminar->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_wc_laminar->replot();
}

//Genera el plot del corte laminar emulsion inversa
void jptwatercut::plot_wc_laminar_ei(){
    ui->chart_wc_laminar_ei->clearGraphs();
    ui->chart_wc_laminar_ei->clearItems();
    ui->chart_wc_laminar_ei->clearPlottables();

    int flag=10;
    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_wc_laminar_ei, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_laminar_ei(QMouseEvent*)));

    connect(ui->chart_wc_laminar_ei, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_laminar_ei()));

    //tengo los dos vectores de corte resistivo y capacitivo
    ui->chart_wc_laminar_ei->addGraph();

    ui->chart_wc_laminar_ei->graph(0)->setPen(QPen(Qt::blue));
    ui->chart_wc_laminar_ei->graph(0)->setName("WC Total Laminar EI %");

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    QVector<double> x(min), y(min);

    int counter=0;

    for (int i=0;i<min;i++) {
        double acum_res=0, acum_cap=0;

        for (int j=0;j<6;j++) {
            acum_cap+=vector_cap_inverse_emulsion_final[i][j] * area_sen[j];
        }
        x[i]= i*5;
        y[i] = acum_cap;

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];

        if(i == 0){
            textTicker->addTick(x[i], date);
        }

        if(event_plot(min,counter)){
            textTicker->addTick(x[i], date);
            counter = 0;
        }
    }

    ui->chart_wc_laminar_ei->xAxis->setTicker(textTicker);
    ui->chart_wc_laminar_ei->graph(0)->setData(x, y);

    ui->chart_wc_laminar_ei->xAxis->setLabel("Date");
    ui->chart_wc_laminar_ei->yAxis->setLabel("% Watercut Laminar Emulsion Inverse");
    // set axes ranges, so we see all data:

    ui->chart_wc_laminar_ei->legend->setVisible(true);

    ui->chart_wc_laminar_ei->xAxis->setRange(-1, min);
    ui->chart_wc_laminar_ei->yAxis->setRange(0, min);
    ui->chart_wc_laminar_ei->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_wc_laminar_ei->replot();
}

//Genera el plot del corte turbulento
void jptwatercut::plot_wc_turb(){
    ui->chart_wc_turbu->clearGraphs();
    ui->chart_wc_turbu->clearItems();
    ui->chart_wc_turbu->clearPlottables();

    int flag=10;
    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_wc_turbu, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_turbulent(QMouseEvent*)));

    connect(ui->chart_wc_turbu, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_turbulent()));

    //tengo los dos vectores de corte resistivo y capacitivo
    //instancia que crea los tickers para las fechas
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    ui->chart_wc_turbu->addGraph();
    ui->chart_wc_turbu->addGraph();
    ui->chart_wc_turbu->addGraph();
    //WC total laminar
    ui->chart_wc_turbu->graph(0)->setPen(QPen(Qt::blue));
    ui->chart_wc_turbu->graph(0)->setName("WC Total Turbulent %");
    //WC resis
    ui->chart_wc_turbu->graph(1)->setPen(QPen(Qt::red));
    ui->chart_wc_turbu->graph(1)->setName("WC Resistivity Turbulent %");
    //WC capa
    ui->chart_wc_turbu->graph(2)->setPen(QPen(Qt::green));
    ui->chart_wc_turbu->graph(2)->setName("WC Capacitivity Turbulent %");

    int counter=0;

    QVector<double> x(min), y(min), y1(min), y2(min);
    for (int i=0;i<min;i++) {
        //double acum_res=0, acum_cap=0;
        double temp_res=0, temp_cap=0;
        for (int j=0;j<6;j++) {
            temp_res+=vector_watercut_resistivity_turb[i][j];
            temp_cap+=vector_watercut_capacitance_turb[i][j];
        }

        x[i]= i*5;

        //x[i]= i*100;
        y[i] = ((temp_res/6)+(temp_cap/6))/2;


        y1[i] = temp_res/6;
        y2[i] = temp_cap/6;

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];

        if(i == 0){
            textTicker->addTick(x[i], date);
        }

        if(event_plot(min,counter)){
            textTicker->addTick(x[i], date);
            counter = 0;
        }
    }

    ui->chart_wc_turbu->xAxis->setTicker(textTicker);
    ui->chart_wc_turbu->graph(0)->setData(x, y);

    ui->chart_wc_turbu->graph(1)->setData(x, y1);

    ui->chart_wc_turbu->graph(2)->setData(x, y2);

    ui->chart_wc_turbu->graph(0)->rescaleAxes();
    ui->chart_wc_turbu->graph(1)->rescaleAxes(true);

    ui->chart_wc_turbu->xAxis->setLabel("Date");
    ui->chart_wc_turbu->yAxis->setLabel("% Watercut Turbulent");

    // label que muestra el chart
    ui->chart_wc_turbu->legend->setVisible(true);
    // set axes ranges, so we see all data:
    ui->chart_wc_turbu->xAxis->setRange(-1, min);
    ui->chart_wc_turbu->yAxis->setRange(0, min);
    ui->chart_wc_turbu->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_wc_turbu->replot();
}

//Genera el plot del corte turbulento laminar
void jptwatercut::plot_wc_turb_lam(){
    ui->chart_wc_turb_lam->clearGraphs();
    ui->chart_wc_turb_lam->clearItems();
    ui->chart_wc_turb_lam->clearPlottables();

    int flag=10;
    //conexion que permite crear los tooltips en la interfaz
    connect(ui->chart_wc_turb_lam, SIGNAL(mouseMove(QMouseEvent*)), this,SLOT(showPointToolTip_turbulent_lam(QMouseEvent*)));

    connect(ui->chart_wc_turb_lam, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel_turbulent_lam()));

    //tengo los dos vectores de corte resistivo y capacitivo
    //instancia que crea los tickers para las fechas
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    ui->chart_wc_turb_lam->addGraph();
    ui->chart_wc_turb_lam->addGraph();
    ui->chart_wc_turb_lam->addGraph();
    //WC total laminar
    ui->chart_wc_turb_lam->graph(0)->setPen(QPen(Qt::blue));
    ui->chart_wc_turb_lam->graph(0)->setName("WC Total Turbulent Laminar %");
    //WC resis
    ui->chart_wc_turb_lam->graph(1)->setPen(QPen(Qt::red));
    ui->chart_wc_turb_lam->graph(1)->setName("WC Resistivity Turbulent Laminar %");
    //WC capa
    ui->chart_wc_turb_lam->graph(2)->setPen(QPen(Qt::green));
    ui->chart_wc_turb_lam->graph(2)->setName("WC Capacitivity Turbulent Laminar %");

    int counter=0;

    QVector<double> x(min), y(min), y1(min), y2(min);
    for (int i=0;i<min;i++) {
        //double acum_res=0, acum_cap=0;
        double temp_res=0, temp_cap=0;
        for (int j=0;j<6;j++) {
            temp_res+=vector_watercut_resistivity_lam_turb[i][j];
            temp_cap+=vector_watercut_capacitance_lam_turb[i][j];
        }

        x[i]= i*5;

        //x[i]= i*100;
        y[i] = ((temp_res/6)+(temp_cap/6))/2;


        y1[i] = temp_res/6;
        y2[i] = temp_cap/6;

        counter++;
        QStringList split = database->get_data_all_nodes_old()[0][i].get_date().split(" ");
        QString date = split[0] + "\n" + split[1];

        if(i == 0){
            textTicker->addTick(x[i], date);
        }

        if(event_plot(min,counter)){
            textTicker->addTick(x[i], date);
            counter = 0;
        }
    }

    ui->chart_wc_turb_lam->xAxis->setTicker(textTicker);
    ui->chart_wc_turb_lam->graph(0)->setData(x, y);

    ui->chart_wc_turb_lam->graph(1)->setData(x, y1);

    ui->chart_wc_turb_lam->graph(2)->setData(x, y2);

    ui->chart_wc_turb_lam->graph(0)->rescaleAxes();
    ui->chart_wc_turb_lam->graph(1)->rescaleAxes(true);

    ui->chart_wc_turb_lam->xAxis->setLabel("Date");
    ui->chart_wc_turb_lam->yAxis->setLabel("% Watercut Turbulent Laminar");

    // label que muestra el chart
    ui->chart_wc_turb_lam->legend->setVisible(true);
    // set axes ranges, so we see all data:
    ui->chart_wc_turb_lam->xAxis->setRange(-1, min);
    ui->chart_wc_turb_lam->yAxis->setRange(0, min);
    ui->chart_wc_turb_lam->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->chart_wc_turb_lam->replot();
}

//Calcula el valor del modelo laminar, esto debido a que cada sensor tiene cierto porcentaje
//el cual atribuye al total de porcentaje del modelo laminar, tanto para resistivo y capacitivo
void jptwatercut::calcule_model_laminar(){

    res_laminar = model->res_laminar_all(vector_watercut_resistivity, min);

    cap_laminar = model->cap_laminar_all(vector_watercut_capacitance, min);

    qDebug()<<"res laminar "<<res_laminar;
    qDebug()<<"cap laminar "<<cap_laminar;

    laminar=true;
    lei=true;
    turbulent=false;
    turb_lam=false;
}

//Calcula el valor del modelo laminar, esto debido a que cada sensor tiene cierto porcentaje
//el cual atribuye al total de porcentaje del modelo laminar emulsion inversa, pero solo para capacitivo
void jptwatercut::calcule_model_laminar_EI(){

    //res_laminar_EI = model->res_laminar_EI_all(vector_watercut_resistivity, min);

    cap_laminar_EI = model->cap_laminar_EI_all(vector_cap_inverse_emulsion_final, min);

    //qDebug()<<"res laminar EI "<<res_laminar_EI;
    qDebug()<<"cap laminar EI "<<cap_laminar_EI;

    ui->line_result_wclei->setText(QString::number(cap_laminar_EI, 'f', 2)+"%");
    on_off_inverse=0;

    plot_wc_laminar_ei();

    laminar = true;
    lei = true;
    turbulent=false;
    turb_lam=false;
}

//Calcula el valor del modelo laminar, esto debido a que cada sensor tiene cierto porcentaje
//el cual atribuye al total de porcentaje del modelo turbulento
void jptwatercut::calcule_model_turbulent(){
    res_turbulent = model->res_turbulent_all(vector_watercut_resistivity_turb, min);

    cap_turbulent = model->cap_turbulent_all(vector_watercut_capacitance_turb, min);

    qDebug()<<"res turbulent "<<res_turbulent;
    qDebug()<<"cap turbulent "<<cap_turbulent;

    ui->line_result_wctr->setText(QString::number(res_turbulent, 'f',2) + "%");
    ui->line_result_wctc->setText(QString::number(cap_turbulent, 'f',2) + "%");
    float final = (res_turbulent+cap_turbulent)/2;

    if(final > 100){
        ui->line_result_wctrt->setText("100%");
    }else{
        ui->line_result_wctrt->setText(QString::number(final, 'f', 2) + "%");
    }

    laminar = false;
    lei = false;
    turbulent=true;
    turb_lam=false;
}

//Calcula el valor del modelo laminar, esto debido a que cada sensor tiene cierto porcentaje
//el cual atribuye al total de porcentaje del modelo turbulento laminar
void jptwatercut::calcule_model_turbulent_lam(){
    res_lam_turbulent = model->res_lam_turbulent_all(vector_watercut_resistivity_lam_turb, min);

    cap_lam_turbulent = model->cap_lam_turbulent_all(vector_watercut_capacitance_lam_turb, min);

    qDebug()<<"res lam turbulent "<<res_lam_turbulent;
    qDebug()<<"cap lam turbulent "<<cap_lam_turbulent;

    //ui->line_result_wctr->setText(QString::number(res_lam_turbulent, 'f',2) + "%");
    //ui->line_result_wctc->setText(QString::number(cap_lam_turbulent, 'f',2) + "%");
    float final = (res_lam_turbulent+cap_lam_turbulent)/2;

    if(final > 100){
        ui->line_result_wcltr->setText("100%");
    }else{
        ui->line_result_wcltr->setText(QString::number(final, 'f', 2) + "%");
    }



    laminar = false;
    lei = false;
    turbulent=false;
    turb_lam=true;
}

//BOTONES===============================================================================
//Boton que acciona el modelo laminar
void jptwatercut::execute(){
    //data_inverse_model();
    regimen_laminar();

    //calculos para mostrar en el lugar de resultados

    ui->line_result_wclr->setText(QString::number(res_laminar,'f',2)+"%");
    ui->line_result_wclc->setText(QString::number(cap_laminar,'f',2)+"%");
    ui->line_result_wclt->setText(QString::number((res_laminar+cap_laminar)/2,'f',2)+"%");
}

//Boton que acciona el modelo laminar emulsion inversa
void jptwatercut::execute_laminar_EI(){
    on_off_inverse=1;
    //compensation_temperature();
    //compensation_salinity();
    data_inverse_model();
    //watercut_resistivity();

}

//Boton que crea los exceles que contiene la informacion de la data RAW, normalizada y valores de corte
//que hayamos calculado
void jptwatercut::create_excel(){
    QString direction_save("");
    if(windows){
        direction_save="C:/JPT";
    }else{
        QStringList parseDirection = ui->line_path->text().split("/");
        direction_save.append("/");
        for (int p_list=0;p_list<parseDirection.count()-1;p_list++){
            direction_save=direction_save+"/"+parseDirection.value(p_list);
        }
    }

    QString name_well = ui->line_name_well->text();
    qDebug()<<direction_save;
    QFile watercutFile(direction_save+"/"+"Watercut_data_RAW_"+name_well+".csv");
    if(watercutFile.open(QIODevice::ReadWrite)){
        QTextStream stream( &watercutFile);
        stream << "time_stamp"<<
                  "\t" << "1-Resistivity"<<"\t"<<"1-Capacitance"<<"\t"<<"2-Resistivity"<<"\t"<<"2-Capacitance"<<
                  "\t" << "3-Resistivity"<<"\t"<<"3-Capacitance"<<"\t"<<"4-Resistivity"<<"\t"<<"4-Capacitance"<<
                  "\t" << "5-Resistivity"<<"\t"<<"5-Capacitance"<<"\t"<<"6-Resistivity"<<"\t"<<"6-Capacitance"<<
                  "\t" << "temperature(RAW)"<<"\t"<<"temperature(ENG)"<< "\t" <<"\n";

        for (int j=0;j<min;j++){
            stream<<database->get_data_all_nodes_old()[0][j].get_date()<<"\t";

            //resistividad-1.2..... capacitancia 1.2.......
            stream<<QString::number(database->get_data_all_nodes_old()[0][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[0][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[1][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[1][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[2][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[2][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[3][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[3][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[4][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[4][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[5][j].get_raw_resis())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[5][j].get_raw_capa())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_raw_tempe())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe(), 'f',2)<<"\t";

            stream<<endl;
        }
    }
    watercutFile.close();

    QFile watercutFile1(direction_save+"/"+"Watercut_data_normalize_"+name_well+".csv");
    if(watercutFile1.open(QIODevice::ReadWrite)){
        QTextStream stream( &watercutFile1);
        stream << "time_stamp"<<
                  "\t" << "1-Resistivity"<<"\t"<<"1-Capacitance"<<"\t"<<"2-Resistivity"<<"\t"<<"2-Capacitance"<<
                  "\t" << "3-Resistivity"<<"\t"<<"3-Capacitance"<<"\t"<<"4-Resistivity"<<"\t"<<"4-Capacitance"<<
                  "\t" << "5-Resistivity"<<"\t"<<"5-Capacitance"<<"\t"<<"6-Resistivity"<<"\t"<<"6-Capacitance"<<
                  "\t" << "temperature(RAW)"<<"\t"<<"temperature(ENG)"<< "\t" <<"\n";

        for (int j=0;j<min;j++){
            stream<<database->get_data_all_nodes_old()[0][j].get_date()<<"\t";

            //resistividad-1.2..... capacitancia 1.2.......
            stream<<QString::number(_node_norm[j][0].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][0].get_cap_norm())<<"\t";

            stream<<QString::number(_node_norm[j][1].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][1].get_cap_norm())<<"\t";

            stream<<QString::number(_node_norm[j][2].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][2].get_cap_norm())<<"\t";

            stream<<QString::number(_node_norm[j][3].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][3].get_cap_norm())<<"\t";

            stream<<QString::number(_node_norm[j][4].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][4].get_cap_norm())<<"\t";

            stream<<QString::number(_node_norm[j][5].get_res_norm())<<"\t";
            stream<<QString::number(_node_norm[j][5].get_cap_norm())<<"\t";

            stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_raw_tempe())<<"\t";
            stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe(), 'f',2)<<"\t";

            stream<<endl;
        }
    }
    watercutFile1.close();

    QFile watercutFile2(direction_save+"/"+"Watercut_percents_"+name_well+".csv");
    if(laminar && lei){
        if(watercutFile2.open(QIODevice::ReadWrite)){
            QTextStream stream( &watercutFile2);
            stream << "time_stamp" <<
                      "\t" << "Temperature °C"<<"\t"<<"Salinity PPM"<<"\t"<<"WC-Resistivity-Laminar %"<<"\t"<<"WC-Capacivite-Laminar %"<<
                      "\t" << "WC-Capacivite-Laminar-EI %"<<"\t"<<"WC-Resistivity-Laminar-Total %"<<"\t"<<"WC-Capacitive-Laminar-Total %"<<
                      "\t"<<"WC-Laminar-Total %"<<"\t"<< "WC-Capacivity-Laminar-EI-Total %"<<"\t" << "Salinity-Total PPM"<<endl;

            bool flag=false;
            float avg_sal=0;

            for (int k=0;k<min;k++) {
                avg_sal+=vector_real_salinity[k];
            }

            for (int j=0;j<min;j++){
                stream<<database->get_data_all_nodes_old()[0][j].get_date()<<"\t";

                stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe(), 'f',2)<<"\t";
                stream<<QString::number(vector_real_salinity[j])<<"\t";

                float avg_wc_laminar_res=0;
                float avg_wc_laminar_cap=0;
                float avg_wc_laminar_cap_EI=0;

                for (int i=0;i<6;i++) {
                    if(vector_watercut_resistivity.count()>0 && vector_watercut_capacitance.count()>0){
                        avg_wc_laminar_res+=vector_watercut_resistivity[j][i];
                        avg_wc_laminar_cap+=vector_watercut_capacitance[j][i];
                    }else{
                        avg_wc_laminar_res = 0;
                        avg_wc_laminar_cap = 0;
                    }
                    avg_wc_laminar_cap_EI+=vector_cap_inverse_emulsion_final[j][i];
                }

                qDebug()<<"wc laminar "<<avg_wc_laminar_res/6;

                stream<<QString::number(avg_wc_laminar_res/6, 'f', 4)<<"\t";
                stream<<QString::number(avg_wc_laminar_cap/6, 'f', 4)<<"\t";
                stream<<QString::number(avg_wc_laminar_cap_EI/6, 'f', 2)<<"\t";

                if(!flag){
                    stream<<QString::number(res_laminar, 'f', 4)<<"\t";
                    stream<<QString::number(cap_laminar, 'f', 4)<<"\t";
                    stream<<QString::number((res_laminar+cap_laminar)/2, 'f', 4)<<"\t";
                    stream<<QString::number(cap_laminar_EI, 'f', 4)<<"\t";
                    stream<<QString::number(avg_sal/min, 'f', 4)<<"\t";
                }

                flag=true;


                stream<<endl;
            }
        }

    }else if(turbulent){
        if(watercutFile2.open(QIODevice::ReadWrite)){
            QTextStream stream( &watercutFile2);
            stream << "time_stamp" <<
                      "\t" << "Temperature °C"<<"\t"<<"Salinity PPM"<<"\t"<<"WC-Resistivity-Turbulent %"<<"\t"<<"WC-Capacivite-Turbulent %"<<
                      "\t" << "WC-Resistivity-Turbulent-Total %"<<"\t"<<"WC-Capacitive-Turbulent-Total %"<<
                      "\t" << "WC-Turbulent-Total %"<<"\t"<< "Salinity-Total PPM"<<endl;

            bool flag=false;
            float avg_sal=0;

            for (int k=0;k<min;k++) {
                avg_sal+=vector_real_salinity[k];
            }

            for (int j=0;j<min;j++){
                stream<<database->get_data_all_nodes_old()[0][j].get_date()<<"\t";

                stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe(), 'f',2)<<"\t";
                stream<<QString::number(vector_real_salinity[j])<<"\t";

                float avg_wc_turb_res=0;
                float avg_wc_turb_cap=0;
                //float avg_wc_laminar_cap_EI=0;

                for (int i=0;i<6;i++) {
                    avg_wc_turb_res+=vector_watercut_resistivity_turb[j][i];
                    avg_wc_turb_cap+=vector_watercut_capacitance_turb[j][i];

                }

                //qDebug()<<"wc laminar "<<avg_wc_laminar_res/6;

                stream<<QString::number(avg_wc_turb_res/6, 'f', 4)<<"\t";
                stream<<QString::number(avg_wc_turb_cap/6, 'f', 4)<<"\t";
                //stream<<QString::number(avg_wc_laminar_cap_EI/6, 'f', 2)<<"\t";

                if(!flag){
                    stream<<QString::number(res_turbulent, 'f', 4)<<"\t";
                    stream<<QString::number(cap_turbulent, 'f', 4)<<"\t";
                    stream<<QString::number((res_turbulent+cap_turbulent)/2, 'f', 4)<<"\t";
                    //stream<<QString::number(cap_laminar_EI, 'f', 4)<<"\t";
                    stream<<QString::number(avg_sal/min, 'f', 4)<<"\t";
                }

                flag=true;


                stream<<endl;
            }
        }
    }else if(turb_lam){
        if(watercutFile2.open(QIODevice::ReadWrite)){
            QTextStream stream( &watercutFile2);
            stream << "time_stamp" <<
                      "\t" << "Temperature °C"<<"\t"<<"Salinity PPM"<<"\t"<<"WC-Resistivity-Turbulent-Laminar %"<<"\t"<<"WC-Capacivite-Turbulent-Laminar %"<<
                      "\t" << "WC-Resistivity-Turbulent-Laminar-Total %"<<"\t"<<"WC-Capacitive-Turbulent-Laminar-Total %"<<
                      "\t" << "WC-Turbulent-Laminar-Total %"<<"\t"<< "Salinity-Total PPM"<<endl;

            bool flag=false;
            float avg_sal=0;

            for (int k=0;k<min;k++) {
                avg_sal+=vector_real_salinity[k];
            }

            for (int j=0;j<min;j++){
                stream<<database->get_data_all_nodes_old()[0][j].get_date()<<"\t";

                stream<<QString::number(database->get_data_all_nodes_old()[6][j].get_eng_tempe(), 'f',2)<<"\t";
                stream<<QString::number(vector_real_salinity[j])<<"\t";

                float avg_wc_turb_res=0;
                float avg_wc_turb_cap=0;
                //float avg_wc_laminar_cap_EI=0;

                for (int i=0;i<6;i++) {
                    if(i!=0){
                        if(i!=3){
                            avg_wc_turb_res+=vector_watercut_resistivity_lam_turb[j][i];
                            avg_wc_turb_cap+=vector_watercut_capacitance_lam_turb[j][i];
                        }
                    }

                }

                //qDebug()<<"wc laminar "<<avg_wc_laminar_res/6;

                stream<<QString::number(avg_wc_turb_res/4, 'f', 4)<<"\t";
                stream<<QString::number(avg_wc_turb_cap/4, 'f', 4)<<"\t";
                //stream<<QString::number(avg_wc_laminar_cap_EI/6, 'f', 2)<<"\t";

                if(!flag){
                    stream<<QString::number(res_lam_turbulent, 'f', 4)<<"\t";
                    stream<<QString::number(cap_lam_turbulent, 'f', 4)<<"\t";
                    stream<<QString::number((res_lam_turbulent+cap_lam_turbulent)/2, 'f', 4)<<"\t";
                    //stream<<QString::number(cap_laminar_EI, 'f', 4)<<"\t";
                    stream<<QString::number(avg_sal/min, 'f', 4)<<"\t";
                }

                flag=true;


                stream<<endl;
            }
        }
    }
    watercutFile2.close();
}




