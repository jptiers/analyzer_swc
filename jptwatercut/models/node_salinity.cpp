#include "node_salinity.h"

node_salinity::node_salinity(){}

void node_salinity::set_real_salinity(int real_salinity){
    this->_real_salinity = real_salinity;
}

int node_salinity::get_real_salinity(){
    return this->_real_salinity;
}

void node_salinity::set_res_salinity(int res_salinity){
    this->_res_salinity = res_salinity;
}

int node_salinity::get_res_salinity(){
    return this->_res_salinity;
}

void node_salinity::set_cap_salinity(int cap_salinity){
    this->_cap_salinity = cap_salinity;
}

int node_salinity::get_cap_salinity(){
    return this->_cap_salinity;
}

void node_salinity::model_real_salinity(int real_salinity){
    this->_real_salinity = real_salinity;
}


