#include "node_temp.h"

node_temp::node_temp(){}

void node_temp::set_res_temp(int res_temp){
    this->_res_temp = res_temp;
}

int node_temp::get_res_temp(){
    return this->_res_temp;
}

void node_temp::set_cap_temp(int cap_temp){
    this->_cap_temp = cap_temp;
}

int node_temp::get_cap_temp(){
    return this->_cap_temp;
}

void node_temp::model_temp(int res_temp, int cap_temp){
    this->_res_temp = res_temp;
    this->_cap_temp = cap_temp;
}
