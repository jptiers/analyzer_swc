#ifndef NODE_SALINITY_H
#define NODE_SALINITY_H


class node_salinity
{
public:
    node_salinity();
    void set_real_salinity(int real_salinity);
    int get_real_salinity();

    void set_res_salinity(int res_salinity);
    int get_res_salinity();

    void set_cap_salinity(int cap_salinity);
    int get_cap_salinity();

    void model_real_salinity(int real_salinity);

private:
    int _real_salinity;
    int _res_salinity;
    int _cap_salinity;
};

#endif // NODE_SALINITY_H
