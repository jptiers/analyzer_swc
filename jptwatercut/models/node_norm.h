#ifndef NODE_NORM_H
#define NODE_NORM_H


class node_norm
{
public:
    node_norm();

    void set_res_norm(int _res_norm);
    int get_res_norm();

    void set_cap_norm(int _cap_norm);
    int get_cap_norm();

    void init_model(int _res_norm, int _cap_norm);

private:
    int res_norm;
    int cap_norm;
};

#endif // NODE_NORM_H
