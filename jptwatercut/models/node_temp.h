#ifndef NODE_TEMP_H
#define NODE_TEMP_H


class node_temp
{
public:
    node_temp();

    void set_res_temp(int res_temp);
    int get_res_temp();

    void set_cap_temp(int cap_temp);
    int get_cap_temp();

    void model_temp(int res_temp, int cap_temp);

private:
    int _res_temp;
    int _cap_temp;
};

#endif // NODE_TEMP_H
