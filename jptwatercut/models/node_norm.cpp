#include "node_norm.h"

node_norm::node_norm(){}

void node_norm::set_res_norm(int _res_norm){
    this->res_norm = _res_norm;
}

int node_norm::get_res_norm(){
    return this->res_norm;
}

void node_norm::set_cap_norm(int _cap_norm){
    this->cap_norm = _cap_norm;
}

int node_norm::get_cap_norm(){
    return this->cap_norm;
}

void node_norm::init_model(int _res_norm, int _cap_norm){
    this->res_norm = _res_norm;
    this->cap_norm = _cap_norm;
}
