QT       += core gui sql script

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++11


#CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

########################################
# Set environment values for Qxlnt. You may use default values.

#msvc:DEFINES+=XLNT_IMPORT

#include(../Qxlnt/Qxlnt.pri)

#msvc{
# message('Visual C++ EXE TYPE')
#CONFIG(debug, debug|release) {
#LIBS+=$${QXLNT_ROOT}debug/QXlnt.lib
#QMAKE_POST_LINK += copy ..\Qxlnt\debug\Qxlnt.dll .\debug\
#} else {
#LIBS+=$${QXLNT_ROOT}release/QXlnt.lib
#QMAKE_POST_LINK += copy ..\Qxlnt\release\Qxlnt.dll .\release\
#}
#}

QXLSX_PARENTPATH=$$PWD/
QXLSX_HEADERPATH=$$PWD/header/
QXLSX_SOURCEPATH=$$PWD/source/
include($$PWD/QXlsx.pri)

SOURCES += \
    logic/jptabssensor.cpp \
    logic/jptcapsensor.cpp \
    logic/jptnormalization.cpp \
    logic/jptressensor.cpp \
    logic/jptsensors.cpp \
    logic/jptmodel.cpp \
    database/jptdatabase.cpp \
    #expression/except.cpp \
    #expression/expr.cpp \
    #expression/func.cpp \
    #expression/funclist.cpp \
    #expression/node.cpp \
    #expression/parser.cpp \
    #expression/vallist.cpp \
    main.cpp \
    jptwatercut.cpp \
    models/_node.cpp \
    models/node_norm.cpp \
    models/node_salinity.cpp \
    models/node_temp.cpp \
    qcustomplot.cpp

HEADERS += \
    logic/jptabssensor.h \
    logic/jptcapsensor.h \
    logic/jptnormalization.h \
    logic/jptressensor.h \
    logic/jptsensors.h \
    logic/jptmodel.h \
    #expression/defs.h \
    #expression/except.h \
    #expression/expr.h \
    #expression/expreval.h \
    #expression/funclist.h \
    #expression/node.h \
    #expression/parser.h \
    #expression/vallist.h \
    database/jptdatabase.h \
    jptwatercut.h \
    models/_node.h \
    models/node_norm.h \
    models/node_salinity.h \
    models/node_temp.h \
    qcustomplot.h

FORMS += \
    jptwatercut.ui



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
