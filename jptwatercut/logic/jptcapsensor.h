#ifndef JPTCAPSENSOR_H
#define JPTCAPSENSOR_H

#include "jptsensors.h"

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compensacion de cuentas por temperatura par valores menores a 8000, que corresponde a Agua.
// y = e1 * pow(raw,2) + e2 * raw  + e3
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_cap_compensate_temp_e1 -0.0278573
#define def_eq_cap_compensate_temp_e2 -13.8214
#define def_eq_cap_compensate_temp_e3  5994.1

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compensacion de cuentas por flujo para valores menores a 8000, que corresponden a Agua
// y = e1 + e2 * raw
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_cap_compensate_flow_e1     6631.1
#define def_eq_cap_compensate_flow_e2    -45.311

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compesacion de cuentas por salinidad para valores menores a 8000, que corresponden a agua.
// y = e1 + (e2 * exp(- raw / e3))
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_cap_compensate_sali_e1 -323.220
#define def_eq_cap_compensate_sali_e2  3891.34
#define def_eq_cap_compensate_sali_e3  1952.78

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para una temperatura de 25C
//-----------------------------------------------------------------------------------------------------------------------------
#define def_count_base_cap_temp  5673

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para una salinidad de 0ppm
//-----------------------------------------------------------------------------------------------------------------------------
#define def_count_base_cap_sali  5673

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para un flujo xx gl
//-----------------------------------------------------------------------------------------------------------------------------
#define def_count_base_cap_flow  5673

//-----------------------------------------------------------------------------------------------------------------------------


class jptcapsensor : public jptsensors{
public:
    jptcapsensor();
    ~jptcapsensor();

    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor resistivo en base a al flujo
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_flow(const double flow, int *delta_cuentas){
        double c_compensate_flow  = (double)(def_eq_cap_compensate_flow_e1 + def_eq_cap_compensate_flow_e2 * flow);
        *delta_cuentas = (int)(def_count_base_cap_flow - c_compensate_flow);
    }

    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor capacitivo en base a la temperatura
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_temp(double temperature, int *delta_cuentas){
        double c_compensate_temp    = (double)(def_eq_cap_compensate_temp_e1 *pow(temperature, 2) +   (def_eq_cap_compensate_temp_e2*temperature)    +   def_eq_cap_compensate_temp_e3);
        *delta_cuentas              = (int)(def_count_base_cap_temp - c_compensate_temp);

    }
    static int calcule_cuentas_temp(double temperature)
    {   double c_compensate_temp    = (double)((def_eq_cap_compensate_temp_e1 * pow(temperature, 2)) +   (def_eq_cap_compensate_temp_e2*temperature)    +   def_eq_cap_compensate_temp_e3);
        int _cuentas= (int)(def_count_base_cap_temp - c_compensate_temp);
        return _cuentas;

    }

    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor resistivo en base a la temperatura
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_sali(double salinity, int *delta_cuentas){
        double c_compensate_sali   = (double) def_eq_cap_compensate_sali_e1 + (def_eq_cap_compensate_sali_e2 * exp(-1 * salinity/def_eq_cap_compensate_sali_e3));
        *delta_cuentas             = (int)(def_count_base_cap_sali - c_compensate_sali);
    }

    static int calcule_delta_sali(double salinity)
    {
        double compensate_salinity=(double) def_eq_cap_compensate_sali_e1 + (def_eq_cap_compensate_sali_e2 * exp(-1 * salinity/def_eq_cap_compensate_sali_e3));
        int _cuentas=(int)(def_count_base_cap_sali - compensate_salinity);
        return _cuentas;

    }

};

#endif // JPTCAPSENSOR_H
