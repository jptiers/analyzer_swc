#ifndef JPTNORMALIZATION_H
#define JPTNORMALIZATION_H

#include <QObject>
#include "database/jptdatabase.h"
#include <QVector>
#include <QThread>
#include "models/node_norm.h"
#include <QScriptEngine>

/*
#include <expression/expreval.h>
using namespace ExprEval;
using namespace std;
*/
class jptnormalization : public QThread
{
    Q_OBJECT
public:
    jptnormalization();
    void run() override;

    jptdatabase *database;
    QList<QString> vector_delta_normalization;
    bool select;
    int min;

    QVector<QList<node_norm>> get_data_norm();

public slots:
    void calcule_norm(jptdatabase *get_all_data, QList<QString> vector_delta_normalization, bool select, int min);
    //void process();

signals:
    void send_data_norm();

private:
    QList<node_norm> list_data_node;
    QVector<QList<node_norm>> data_node;
};

#endif // JPTNORMALIZATION_H
