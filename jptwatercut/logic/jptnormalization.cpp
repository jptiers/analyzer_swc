#include "jptnormalization.h"
#include <QDebug>

jptnormalization::jptnormalization(){}

void jptnormalization::run(){
    calcule_norm(database, vector_delta_normalization, select, min);
}

void jptnormalization::calcule_norm(jptdatabase *get_all_data, QList<QString> vector_delta_normalization, bool select, int min){
    qDebug()<<"normalization active";

    data_node.clear();

    //qRegisterMetaType<QVector<QList<node_norm>>>();

    for (int i=0;i<min;i++) {

        list_data_node.clear();
        for (int j=0;j<6;j++) {
            int raw_cap=0, raw_resis=0, norm_raw_resis=0, norm_raw_cap=0;
            if(select){

                /*
                ValueList       lv_vlist;
                FunctionList    lv_flist;
                lv_vlist.AddDefaultValues();
                lv_flist.AddDefaultFunctions();

                Expression lv_resis;
                lv_resis.SetValueList(&lv_vlist);
                lv_resis.SetFunctionList(&lv_flist);

                Expression lv_capa;
                lv_capa.SetValueList(&lv_vlist);
                lv_capa.SetFunctionList(&lv_flist);

                */

                /**
                 * RECORDAR:
                 * Definimos el script para leer la funcion desde un string (campos de texto)
                 * luego pasamos el engine con evaluate para scriptValue y asi obtener el resultado
                 */

                QScriptEngine engine_functions_res;
                QScriptValue master_value_res;

                QScriptEngine engine_functions_cap;
                QScriptValue master_value_cap;

                raw_resis = get_all_data->get_data_all_nodes_old()[j][i].get_raw_resis();
                raw_cap = get_all_data->get_data_all_nodes_old()[j][i].get_raw_capa();

                QString replace_res = vector_delta_normalization[j].replace("X", QString::number(raw_resis));
                QString replace_cap = vector_delta_normalization[j+6].replace("X", QString::number(raw_cap));

                master_value_res = engine_functions_res.evaluate(replace_res);
                master_value_cap = engine_functions_cap.evaluate(replace_cap);

                qDebug()<<"MASTER "<< master_value_res.toNumber();

                norm_raw_resis= master_value_res.toNumber();
                norm_raw_cap = master_value_cap.toNumber();

                //Seteo valor normalizao
                node_norm norm;

                norm.init_model(norm_raw_resis, norm_raw_cap);
                qDebug()<<i<<"resis "<< norm_raw_resis<< "cap "<< norm_raw_cap << " "<<j;
                list_data_node.append(norm);

            }else{
                //Variables temporales raw
                raw_resis = get_all_data->get_data_all_nodes_old()[j][i].get_raw_resis();
                raw_cap = get_all_data->get_data_all_nodes_old()[j][i].get_raw_capa();
                //Seteo valor normalizado

                node_norm norm;

                norm.init_model(raw_resis, raw_cap);
                //qDebug()<<i<<"resis "<< raw_resis<< "cap "<< raw_cap << " "<<j;
                list_data_node.append(norm);
            }
        }
        data_node.append(list_data_node);
    }
    emit send_data_norm();
}

QVector<QList<node_norm>> jptnormalization::get_data_norm(){
    return data_node;
}

