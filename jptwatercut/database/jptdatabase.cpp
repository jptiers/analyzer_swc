#include "jptdatabase.h"
#include <QtDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>


const QString gv_list_name_tables[7] = {"Node_0001","Node_0002","Node_0003","Node_0004","Node_0005","Node_0006","Node_0007"};
const QString list_name_sheets[7] = {"Node_001","Node_002","Node_003","Node_004","Node_005","Node_006","Node_007"};
//Instancia de la clase principal, no se puede hacer desde el .h por problemas de compilacion
//*********************************************************************************************************************************************************
// Constructor
//*********************************************************************************************************************************************************
jptdatabase::jptdatabase(){}
//*********************************************************************************************************************************************************
// Inicializa la conexion con la base de datos
//*********************************************************************************************************************************************************

void jptdatabase::run(){
    process_run();
}

void jptdatabase::process_run(){
    charge_data_on_memory(startDate, endDate);
}

void jptdatabase::init_database_connection(QString name_data_base){
    /*
    if(db.open()){
        db.close();
    }
    */
    //This process only seup he database system.
    QStringList split = name_data_base.split(".");
    data_node.clear();
    bool _end = false;

    if(split[1] != "db"){
        //si es diferente se supone que es excel
        Document xlsxR(name_data_base);

        QDateTime start = QDateTime::fromString(startDate, "dd/MM/yyyy hh:mm:ss");
        QDateTime end = QDateTime::fromString(endDate,  "dd/MM/yyyy hh:mm:ss");



        for (int i=0; i<7;i++){
            list_data_node.clear();
            xlsxR.selectSheet(list_name_sheets[i]);
            // load excel file
            if (xlsxR.load()){
                int j=2;
                while (true) {
                    int row = j;

                    // get cell pointer.
                    Cell* cell_resis = xlsxR.cellAt(row, 1);
                    Cell* cell_eng_resis = xlsxR.cellAt(row, 2);
                    Cell* cell_cap = xlsxR.cellAt(row, 3);
                    Cell* cell_eng_cap= xlsxR.cellAt(row, 4);
                    Cell* cell_date = xlsxR.cellAt(row, 5);

                    if (cell_resis != NULL || cell_eng_resis != NULL || cell_cap != NULL
                         || cell_eng_cap != NULL || cell_date != NULL){

                            _node temp_model;
                            if(list_name_sheets[i]=="Node_007"){
                                Cell* cell_raw_temp = xlsxR.cellAt(row, 1);
                                Cell* cell_eng_temp = xlsxR.cellAt(row, 2);
                                Cell* cell_date = xlsxR.cellAt(row, 3);

                                QDateTime date = QDateTime::fromString(cell_date->value().toString(),  "dd/MM/yyyy hh:mm:ss");

                                std::time_t bar = cell_date->readValue().toDouble();

                                qDebug()<<bar;
                                if(date >= start && date <= end){
                                    temp_model.init_model_temp(cell_raw_temp->value().toInt(),
                                                               cell_eng_temp->value().toFloat(),
                                                               0,0,0,0,0,0,0,0,0,0);

                                    list_data_node.append(temp_model);
                                }

                                _end = true;
                            }else{
                                QDateTime date = QDateTime::fromString(cell_date->value().toString(),  "dd/MM/yyyy hh:mm:ss");
                                if(date >= start && date <= end){
                                    qDebug()<<date.toString();
                                    temp_model.init_model(cell_resis->value().toInt(), cell_eng_resis->value().toInt(),
                                                          cell_eng_cap->value().toInt(), cell_eng_cap->value().toInt(),
                                                          cell_date->value().toString()
                                                          ,0,0,0,0,0,0,0,0,0,0);
                                    list_data_node.append(temp_model);
                                }

                            }
                            qDebug()<<cell_resis->value().toString();

                    }else{
                        qDebug()<<"=========="<<list_name_sheets[i];
                        break;
                    }
                    j++;
                }
            }
            data_node.append(list_data_node);
            qDebug()<<list_data_node.count();

        }
    }else{
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(name_data_base);
    }

    if(db.open()){
        //emit sendMsg("Database upload");
        qDebug()<<"leyo";
    }
    else{
        //emit sendMsg("Database error");
        qDebug()<<"no leyo";
    }
}

void jptdatabase::charge_data_on_memory(QString startDate, QString endDate)
{
    //Se obitene toda la data de la base de datos para procesarla
    try {
        get_all_data(startDate, endDate);
        qDebug()<<"obtuvo toda la data";
    } catch (...) {
        qDebug()<<"error al obtener toda la data";
        emit sendMsg("Error upload all data from database");
    }

}

void jptdatabase::get_all_data(QString startDate, QString endDate){
    data_node.clear();
    bool end = false;
    for (int i=0;i<7;i++) {
        QString new_query;

        //Validamos las fechas para hacer la consulta de la data en la base de datos
        //QStringList split = endDate.split("");
        if(startDate != "2000/01/01 00:00:00" && endDate != "2000/01/01 23:59:59"
                && startDate != "" && endDate != ""){
            new_query.append("select * from " + gv_list_name_tables[i] +
                             " WHERE date BETWEEN '"+ startDate +"' and '"+ endDate +"' order by date ASC;");
        }else{
            //new_query.append("select id, m_datetime, m_data, m_device from Nodex;");
        }
        qDebug()<<new_query;


        QSqlQuery create;
        create.prepare(new_query);
        create.exec();

        list_data_node.clear();

        while(create.next()){
            QString raw_resis = create.value(1).toString();
            QString eng_resis = create.value(2).toString();
            QString raw_capa = create.value(3).toString();
            QString eng_capa = create.value(4).toString();
            QString date = create.value(5).toString();

            _node temp_model;
            if(gv_list_name_tables[i]=="Node_0007"){
                temp_model.init_model_temp(raw_resis.toInt(), eng_resis.toFloat(),0,0,0,0,0,0,0,0,0,0);
                list_data_node.append(temp_model);
                end = true;
            }else{
                temp_model.init_model(raw_resis.toInt(), eng_resis.toInt(), raw_capa.toInt(), eng_capa.toInt(), date,0,0,0,0,0,0,0,0,0,0);
                list_data_node.append(temp_model);
            }
        }
        data_node.append(list_data_node);
        qDebug()<<list_data_node.count();

        //emit sendMsg("Total count of the elements " + QString::number(data_nodex.count()));
    }
    qDebug()<<data_node.count();
    msleep(2000);
    if(end){
        db.close();
        emit kill_thread();
    }
}

//lo mas optimo es crear un elemento o clase denominado data
//data tendria 4 elementos los cuales seriam id, tiempo, data, device
//luego se genera solamente una lista de de datos de la forma QList<data> my_data.

QVector<QList<_node>> jptdatabase::get_data_all_nodes_old(){
    return data_node;
}

//*********************************************************************************************************************************************************
// Destructor
//*********************************************************************************************************************************************************
jptdatabase::~jptdatabase(){
    db.close();
}
