#ifndef JPTDATABASE_H
#define JPTDATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QThread>
#include "models/_node.h"
#include <QScriptEngine>

#include <iostream> // STL


/*
#include <xlnt/xlnt.hpp> // xlnt
#include <QCoreApplication> // Qt
*/

#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"
using namespace QXlsx;

class jptdatabase : public QThread{
    Q_OBJECT

public:

    jptdatabase();
    ~jptdatabase();

    void run();
    void process_run();
    void init_database_connection(QString name_data_base);

    void charge_data_on_memory(QString startDate, QString endDate);
    void get_all_data(QString startDate, QString endDate);

    //getters de continuidad
    QVector<QList<_node>> get_data_all_nodes_old();
    QString startDate;
    QString endDate;

public slots:

//Los procesos reciben informacion por medio de variables y realizan un procedimiento con esas variables.
//los slots, reciben señales, alamacenan informacion y realizan procesos
    //por ende para manejar un slot, es necesario manejarlo por medio de sistma connect.
    //los slots pueden manejar diferentes procesos internos en ellos.

signals:
    void sendMsg(QString msg);
    void kill_thread();

private:
    //This us help to fill with data from each
    QList<_node> list_data_node;
    QVector<QList<_node>> data_node;
    QSqlDatabase db;
};

#endif // JPTDATABASE_H
