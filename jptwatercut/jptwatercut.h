#ifndef JPTWATERCUT_H
#define JPTWATERCUT_H

#include <QMainWindow>
#include <QObject>
#include <QDateTime>
#include "database/jptdatabase.h"
#include "logic/jptabssensor.h"
#include "logic/jptcapsensor.h"
#include "logic/jptmodel.h"
#include "logic/jptressensor.h"
#include "logic/jptsensors.h"
#include "qcustomplot.h"
#include <QThread>
#include "logic/jptnormalization.h"
#include "models/node_norm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class jptwatercut; }
QT_END_NAMESPACE

class jptwatercut : public QMainWindow
{
    Q_OBJECT

public:
    jptwatercut(QWidget *parent = nullptr);
    ~jptwatercut();
    //void charge_table_old();
    void charge_line_norm();
    void validator_line_norm();
    void fill_combo_salinity();
    int min;
    void charge_models_tempe_pres();
    void read_txt_values_calibration();
    void read_txt_values_high_low_wc();
    void read_txt_delta_normalization();
    void read_txt_inverse();
    void read_txt_inverse_values();
    void read_txt_correct_sali();
    void read_txt_correct_temp();
    void read_txt_correct_tempval();
    //void normalization_data();
    //void compensation_temperature();
    //void compensation_salinity();
    void data_inverse_model();
    void model_percent();
    void watercut_resistivity();
    void watercut_capacitive();
    void watercut_capacitive_EI();
    void watercut_resistivity_turb();
    void watercut_capacitive_turb();
    void watercut_resistivity_turb_lam();
    void watercut_capacitive_turb_lam();
    void regimen_laminar();

    void calcule_model_laminar();
    void calcule_model_laminar_EI();
    void calcule_model_turbulent();
    void calcule_model_turbulent_lam();

    int subs_values(int value1, int value2);
    bool event_plot(int min, int counter);

    void fill_all();
    void clean_all();

    //void plot_raw_data();
    //void plot_norm_data();
    void plot_salinity();
    void plot_wc_laminar();
    void plot_wc_laminar_ei();
    void plot_wc_turb();
    void plot_wc_turb_lam();


public slots:
    void select_combo_salinity(QString curve);
    void select_combo_pipe(QString value);
    void active_less_gas();
    void calculate_caudal();
    void select_db();
    void test_db();
    void get_datetime_init(QDateTime init);
    void get_datetime_end(QDateTime end);

    void charge_table_old();

    void charge_norm_data();
    void get_norm();
    void normalization_data();

    void plot_raw_data();
    void plot_norm_data();
    void active_thread_temp();
    void active_thread_salinity();

    void compensation_temperature();
    void compensation_salinity();

    void execute();
    void execute_laminar_EI();
    void create_excel();
    void regimen_turbulent();
    void regimen_turbulent_laminar();

    void showPointToolTip_raw(QMouseEvent *event);
    void mouseWheel_raw();

    void showPointToolTip_norm(QMouseEvent *event);
    void mouseWheel_norm();

    void showPointToolTip_salinity(QMouseEvent *event);
    void mouseWheel_salinity();

    void showPointToolTip_laminar(QMouseEvent *event);
    void mouseWheel_laminar();

    void showPointToolTip_laminar_ei(QMouseEvent *event);
    void mouseWheel_laminar_ei();

    void showPointToolTip_turbulent(QMouseEvent *event);
    void mouseWheel_turbulent();

    void showPointToolTip_turbulent_lam(QMouseEvent *event);
    void mouseWheel_turbulent_lam();

    void save_txt_delta_normalization();

    void kill_thread();

signals:
    void end_thread_old_table();
    void end_thread_normalize();
    void end_thread_plot_raw();
    void end_thread_plot_norm();
    void end_thread_temperature();
    void end_thread_salinity();

private:
    Ui::jptwatercut *ui;
    QDateTime _init, _end;
    jptdatabase *database;
    jptmodel *model;
    jptsensors *sensors;
    jptabssensor *abs_sensors;
    jptcapsensor *cap_sensors;
    jptressensor *res_sensors;
    jptnormalization *normalization;

    QStandardItemModel *model_old;
    QStandardItemModel *model_norm;



    //Headers para la tabla.
    enum Columns_table_old{
      RESIS_1, CAPA_1, RESIS_2, CAPA_2, RESIS_3, CAPA_3, RESIS_4, CAPA_4, RESIS_5, CAPA_5, RESIS_6, CAPA_6, TEMP_RAW, TEMP_ENG
    };
};
#endif // JPTWATERCUT_H
